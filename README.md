# application d'inscritpion du Camp Climat

[Site web public](https://alternatiba.frama.io/camp-climat-inscription)

## Comment développer
Le back par deafaut est le heroku de production
pour setter un back différent créer une variable d'environment
```
export CAMP_CLIMAT_API_URL=<url>
```

```
yarn
yarn dev
```

[open](http://localhost:8080)

## Comment déployer
Chaque push sur la branch master sont déployés par [gitlab-ci](.gitlab-ci.yml).
