import GenericElement from '../../core/genericElement.js';
import view from './view.html';
import Moment from 'moment'
export default class Credits extends GenericElement {
  constructor() {
    super(view);

    this.elements = {
    };
  }

  connectedCallback() {
    super.connectedCallback();
  }

}
window.customElements.define('x-credits', Credits);
