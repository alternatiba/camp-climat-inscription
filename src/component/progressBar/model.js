import { scale } from 'chroma-js';
import GenericElement from '../../core/genericElement.js';
import view from './view.html';

const MAX = 100;

export default class ProgressBar extends GenericElement {
  static get observedAttributes() {
    return ['value'];
  }

  value = 0;
  realValue = 0;

  constructor() {
    super(view);

    this.elements = {
      progressBarContainer: this.shadowRoot.querySelector('#progress-container'),
      progressBarValue: this.shadowRoot.querySelector('#progress-value'),
      progressBarRealValue: this.shadowRoot.querySelector('#progress-real-value'),
    };

    this.setValue(this.getAttribute('value'));
  }

  setValue(value) {
    value = Number(value);
    this.realValue = value;
    if (value > MAX) {
      this.value = MAX;
    } else {
      this.value = value;
    }
  }

  updateUI() {
    const percentWidth = `${this.value}%`;
    const ratio = this.value / 100;

    const colorScale = scale(['red', 'yellow', '#087f49']);

    this.elements.progressBarValue.style.width = percentWidth;
    this.elements.progressBarValue.style.backgroundColor = colorScale(ratio);
    if (this.realValue >= 100) {
      this.elements.progressBarRealValue.innerHTML = `${this.realValue}% on t'attend le jour J`;
    } else {
      this.elements.progressBarRealValue.innerHTML = `${this.realValue}%`;
    }

  }

  attributeChangedCallback(attrName, oldVal, newVal) {
    if (attrName === 'value') {
      this.setValue(newVal);
      this.updateUI();
    }
  }

  connectedCallback() {
    super.connectedCallback();

    this.updateUI();
  }
}
window.customElements.define('x-progress-bar', ProgressBar);
