import Moment from 'moment'
import GenericElement from '../../core/genericElement.js';
import view from './view.html';

export default class RegistrationGuide extends GenericElement {
  constructor() {
    super(view);
  }

  connectedCallback() {
    super.connectedCallback();
  }

}
window.customElements.define('x-registration-guide', RegistrationGuide);
