import GenericElement from '../../../core/genericElement.js';
import view from './view.html';
export default class DaysList extends GenericElement {
  constructor() {
    super(view);
    this.subscribe({
      channel: 'daysWithInfo',
      topic: 'loaded',
      callback: (data) => {
        // console.log('daysWithInfo',data);
        this.setData(data);
      }
    });

    // console.log('css',css.toString());

    this.elements = {
      root: this.shadowRoot.getElementById('days-list'),
    };
  }

  connectedCallback() {
    super.connectedCallback();
    this.publish({
      channel: 'days',
      topic: 'loadAll'
    });
  }

  setData(data) {
    // console.trace();
    var documentFragment = document.createDocumentFragment();
    data.days.forEach((item, index) =>{
      let day = document.createElement('li');
      let link = document.createElement('a');
      link.href = `${data.baseUrl}day=${index + 1}`;
      //console.log('day', item.format(), item);
      link.textContent = item.day.format('D MMMM');
      if(item.specialBuild){
        day.classList.add('specialBuild');
      }
      if (index === data.selectedDayIndex - 1) {
        day.classList.add('selected');
      }
      day.appendChild(link);
      documentFragment.appendChild(day);
    })
    this.elements.root.innerHTML = '';
    this.elements.root.appendChild(documentFragment);
  }

}
window.customElements.define('x-days-list', DaysList);
