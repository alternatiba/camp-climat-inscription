import GenericElement from '../../../../core/genericElement.js';
import view from './view.html';
import ExpandableText from './expandableText/model.js';
import tippy from 'tippy.js'

export default class Session extends GenericElement {
  constructor() {
    super(view);
    this.assetsContext = require.context('../../../../assets', false, /\.(png|jpe?g|svg)$/);
    this.elements = {
      root: this.shadowRoot.querySelector('.session'),
      title: this.shadowRoot.querySelector('.title'),
      location: this.shadowRoot.querySelector('.location'),
      trainer: this.shadowRoot.querySelector('.trainer'),
      refererLabel: this.shadowRoot.querySelector('.refererLabel'),
      abstract: this.shadowRoot.querySelector('.abstract'),
      important: this.shadowRoot.querySelector('.important'),
      slots: this.shadowRoot.querySelector('.slots'),
      error: this.shadowRoot.querySelector('.error'),
      from: this.shadowRoot.querySelector('.from'),
      to: this.shadowRoot.querySelector('.to'),
      registerButton: this.shadowRoot.querySelector('.register'),
      curriculums: this.shadowRoot.querySelector('[name="curriculums"]'),
      domain: this.shadowRoot.querySelector('[name="domain"]'),
      expandableText: this.shadowRoot.querySelector('#expandableText'),
      info: this.shadowRoot.querySelector('[name="info"]'),
    }

    this.subscribe({
      channel: 'session',
      topic: 'changed',
      callback: (data) => {
        // console.warn("session changed", data);
        if (data.keySlot === this.session.keySlot) {
          this.render(data);
        }
      }
    });

    this.subscribe({
      channel: 'users',
      topic: 'changed.connected',
      callback: (data) => {
        this.applyConnected(data);
      }
    });

    this.subscribe({
      channel: 'users',
      topic: 'changed',
      callback: (data) => {
        this.applyUser(data);
      }
    });

    this.subscribe({
      channel: 'session',
      topic: 'registration.failed',
      callback: (data) => {

        if (data.id === this.keySlot) {
          if (data.reason != undefined) {
            switch (data.reason) {
              case "someSessionOutOfDate":
                this.elements.error.textContent = "une partie de cette activite est hors de tes jours de présence";
                break;
              case "someSessionInConflict":
                this.elements.error.textContent = "une partie de cette activite est en conflit avec une autre à laquelle tu est inscrit";
                break;
              case "busyVolume":
                this.elements.error.textContent = "cette activité est déjà complète";
                break;
              case "benevolPaid":
                  this.elements.error.textContent = "Tu ne peux pas te désinscrire d'un benevolat une fois sur le camp. Une personne Administratrice peut le faire à l'Office du Militantisme";
                  break;
              default:

            }

          } else {
            this.elements.error.textContent = data.message || "Erreur interne";
          }
          this.elements.root.classList.add('conflict');
          this.elements.registerButton.classList.add('disabled');
          this.elements.registerButton.textContent = 'Conflit';
        }
      }
    });
    this.subscribe({
      channel: 'session',
      topic: 'highlight',
      callback: (data) => {
        if (data.id === this.keySlot) {
          this.elements.root.classList.add("highlighted");
          setTimeout(() => {
            this.elements.root.classList.remove("highlighted");
          }, 400);
        }
      }
    });
  }

  setId(id) {
    this.id = id;
  }

  connectedCallback() {
    // super.connectedCallback();
    this.publish({
      channel: 'users',
      topic: 'get.connected'
    })
    this.publish({
      channel: 'users',
      topic: 'get'
    })
    this.elements.registerButton.addEventListener('click', (event) => this.onClick());
  }

  mixConnectedAndUser(){
    if(!this.userConnected.isAdmin && this.user.paid===true && this.session.domain==="BENEVOL" && this.session.registered===true){
      this.elements.registerButton.classList.add('disabled');
    }
  }


  applyUser(data){
    this.user=data;
    if(this.userConnected!=undefined){
      this.mixConnectedAndUser();
    }
  }

  applyConnected(data){
    this.userConnected=data;
    if(data.isAdmin){
      this.elements.info.classList.remove('hide');
    }
    if(this.user!=undefined){
      this.mixConnectedAndUser();
    }
  }

  render(data, counter) {
    //console.log('session', data.start.format(), data.start.format('HH mm'), data.start);
    // console.log('render',data);
    this.session = data;

    this.id = data.id;
    this.keySlot = data.keySlot;
    this.elements.title.textContent = data.title;
    this.elements.location.textContent = data.location;
    this.elements.refererLabel.textContent = data.refererLabel.concat(data.trainers.length > 1 ? '.s' : '');

    this.elements.trainer.textContent = data.trainers.map(s => s.name).join(', ');
    this.elements.abstract.textContent = data.abstract;

    if (data.colorDomain) {
      this.elements.root.style = `border: 3px solid ${data.colorDomain};`;
    }

    if (data.important) {
      this.elements.important.textContent = data.important.replace(/(?:\r\n|\r|\n)/g, '. ');
    }
    this.elements.from.textContent = this.formatTime(data.start);
    this.elements.to.textContent = this.formatTime(data.end);



    while (this.elements.domain.firstChild) {
      this.elements.domain.removeChild(this.elements.domain.firstChild);
    }
    if (data.assetDomain != undefined) {
      let path = './' + data.assetDomain;
      let imgData = this.assetsContext(path);
      let img = document.createElement('img')
      img.setAttribute('src', imgData);
      img.style = `border-bottom: 3px solid ${data.colorDomain};border-right: 3px solid ${data.colorDomain};`;
      this.elements.domain.appendChild(img);
      tippy(img, {
        content: data.descDomain,
      })
    }

    while (this.elements.curriculums.firstChild) {
      this.elements.curriculums.removeChild(this.elements.curriculums.firstChild);
    }
    for (let curriculum of data.curriculums) {
      let path = './' + curriculum.asset;
      let imgData = this.assetsContext(path);
      let imgContainer = document.createElement('div');
      let img = document.createElement('img');

      img.setAttribute('src', imgData);
      // console.log(curriculum);
      img.style = `border-bottom: 3px solid ${curriculum.color} ;border-left: 3px solid ${curriculum.color} ;`;
      if (!curriculum.isCuriculumUser) {
        imgContainer.classList.add('greyFilter');
      }

      imgContainer.appendChild(img);

      this.elements.curriculums.classList
      this.elements.curriculums.appendChild(imgContainer);
      tippy(img, {
        content: curriculum.desc,
      })
    }
    // this.elements.curriculums.textContent=data.curriculums.map(c=>c.asset).join(',');

    if (data.isTrainer) {
      this.elements.slots.textContent = 'Tu es référent.e de cette activité';
      this.elements.registerButton.textContent = data.refererLabel;
      this.elements.registerButton.classList.add('registered');
      this.elements.info.classList.remove('hide');
    } else if (data.registered) {
      this.elements.registerButton.classList.add('registered');
      if (data.isInscriptionFree == true) {
        this.elements.slots.textContent = 'Activité non bloquante';
        this.elements.registerButton.textContent = 'Tu t\'es noté cette activité';
      } else {
        this.elements.slots.textContent = 'Tu es inscrit.e à cette activité';
        this.elements.registerButton.textContent = 'Je me désinscris';
      }
      this.elements.registerButton.classList.remove('disabled');
    } else {
      this.elements.registerButton.classList.remove('registered');

      if (data.isFull) {
        this.elements.slots.textContent = `Complet [${data.busyVolume}/${data.volume}]`;
        this.elements.registerButton.textContent = 'Complet';
        this.elements.registerButton.classList.add('disabled');
      } else {
        this.elements.registerButton.classList.remove('disabled');

        if (data.isInscriptionFree == true) {
          this.elements.slots.textContent = `Activité non bloquante, Il reste ${data.freeSlots} place${data.freeSlots>1?'s':''} [${data.busyVolume}/${data.volume}]`;
          this.elements.registerButton.textContent = 'Je me le note';
        } else {
          this.elements.slots.textContent = `Il reste ${data.freeSlots} place${data.freeSlots>1?'s':''} [${data.busyVolume}/${data.volume}]`;
          this.elements.registerButton.textContent = 'Je m\'inscris';
        }

      }

      if (data.conflicting) {
        this.elements.root.classList.add('conflict');
        this.elements.registerButton.classList.add('disabled');
        this.elements.registerButton.textContent = 'Conflit';
      } else {
        this.elements.root.classList.remove('conflict');
      }



    }
    this.elements.info.setAttribute('href', this.elements.info.getAttribute('href-base') + data.keySlot);
    if(!this.elements.info.firstChild){
      let imgData = this.assetsContext('./list.png');
      // this.elements.info.setAttribute('href', this.elements.info.getAttribute('href') + data.keySlot);
      // let imgContainer = document.createElement('div');
      let img = document.createElement('img');
      img.setAttribute('src', imgData);
      this.elements.info.appendChild(img);
    }
    // while (this.elements.domain.firstChild) {
    //   this.elements.domain.removeChild(this.elements.domain.firstChild);
    // }
    this.elements.error.textContent = "";
    // console.log('end render',counter);
  }

  formatTime(date) {
    return date.format('HH:mm');
    //return new Date(Date.parse(date)).toLocaleTimeString("fr-FR", {'hour': '2-digit', 'minute': '2-digit'});
  }

  onClick() {
    if (this.session.registered) {
      if(this.session.isTrainer === true){
        this.elements.error.textContent = `Tu ne peux pas te désinscrire en tant que référent.e de cette activité`;
      }
      else if (!this.userConnected.isAdmin && this.user.paid==true && this.session.domain==="BENEVOL") {
        this.elements.error.textContent = `Tu ne peux pas te désinscrire d'un benevolat une fois sur le camp. Une personne Administratrice peut le faire à l'Office du Militantisme`;
      }
      else {
        this.publish({
          channel: 'session',
          topic: 'unregister',
          'data': this.keySlot
        });
      }

    } else {
      if (this.session.conflicting) {
        this.elements.error.textContent = `Tu es déjà inscrit.e à l'activité ${this.session.conflicting.title} ${this.session.conflicting.start.format("dddd Do MMMM HH:mm")}`;
        this.publish({
          channel: 'session',
          topic: 'highlight',
          data: this.session.conflicting
        });
      } else if (this.session.isFull) {
        this.elements.error.textContent = `Désolé, l'activité est complète.`;
      } else if (this.session.isTrainer === false) {
        this.elements.registerButton.textContent = '...';
        this.publish({
          channel: 'session',
          topic: 'register',
          'data': this.keySlot
        });
      }
    }
  }
}
window.customElements.define('x-session', Session);
