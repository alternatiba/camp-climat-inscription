import { scale } from 'chroma-js';
import GenericElement from '../../../../../core/genericElement.js';
import view from './view.html';
const MAX = 100;

export default class ExpandableText extends GenericElement {
  constructor() {
    super(view,[]);

    this.elements = {
      root: this.shadowRoot.querySelector('.container'),
      specialText: this.shadowRoot.querySelector('.special-text'),
      button: this.shadowRoot.querySelector('button'),
      slot: this.shadowRoot.querySelector('slot'),
    };
  }

  render() {
    // console.warn('ALLO');
    // var length = 0;
    let height=this.elements.specialText.offsetHeight;
    // console.log(this.elements.specialText.offsetHeight,this.maxHeightExpand);
    // this.childNodes.forEach( (n) => {
    //   length += n.textContent.length;
    //   // length += n.innerHTML.length;
    //   // console.log(n.innerHTML);
    //   console.log(length);
    // });
    // console.warn(height);
    if (height > this.maxHeightExpand) {
      this.elements.specialText.classList.remove('-expanded');
      this.shadowRoot.addEventListener('click', (e) => {
        if (this.elements.specialText.classList.contains("-expanded")) {
          this.elements.specialText.classList.remove('-expanded');
          this.elements.button.textContent = '+';
        } else {
          this.elements.specialText.classList.add('-expanded');
          this.elements.button.textContent = '-';
        }
      });
    } else {
      // this.elements.specialText.classList.add('-expanded');
      this.elements.root.classList.add('disabled');
    }
  }

  attributeChangedCallback(attrName, oldVal, newVal) {
    super.attributeChangedCallback();
    // this.render();
    // this[attrName]=newVal;
  }

  connectedCallback() {
    // super.connectedCallback();
    this.maxHeightExpand=parseInt(this.getAttribute('maxHeightExpand'));
    this.elements.slot.addEventListener('slotchange', (e)=> {
      this.render();
    })
    let injectedStyle = document.createElement('style');
    injectedStyle.appendChild(document.createTextNode(`.special-text {
        max-height: ${this.maxHeightExpand-15}px;
      }`));
    this.shadowRoot.appendChild(injectedStyle);
  }
}
window.customElements.define('x-expandable-text', ExpandableText);
