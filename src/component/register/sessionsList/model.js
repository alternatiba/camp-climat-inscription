import GenericElement from '../../../core/genericElement.js';
import view from './view.html';
import session from './session/model.js';
export default class SessionsList extends GenericElement {
  constructor() {
    super(view,[]);
    this.subscribe({
      channel: 'sessions',
      topic: 'loaded',
      callback: (data) => {
        this.setData(data);
      }
    });
    this.subscribe({
      channel: 'days',
      topic: 'selected',
      callback: (data) => {
        this.setDay(data);
      }
    });

    this.subscribe({
      channel: 'daysWithInfo',
      topic: 'loaded',
      callback: (data) => {
        let selectedDay=data.days[data.selectedDayIndex-1];
        if(selectedDay!=undefined){
          this.setDay(selectedDay)
        }
      }
    });


    this.elements = {
      root: this.shadowRoot.getElementById('sessions-list'),
      infoPanel: this.shadowRoot.getElementById('infoPanel'),
      emptyPanel : this.shadowRoot.getElementById('emptyPanel'),
    };
  }

  connectedCallback() {
    super.connectedCallback();
    this.publish({
      channel: 'sessions',
      topic: 'loadAll'
    });

  }

  setDay(data) {
    if(data!=undefined){
      if(data.specialBuild==true){
        this.elements.infoPanel.classList.remove('hide')
      }else{
        this.elements.infoPanel.classList.add('hide')
      }
    }

  }

  setData(data) {
    // let counter=1;
    while (this.elements.root.firstChild) {
      this.elements.root.removeChild(this.elements.root.firstChild);
    }
    if(data.length==0){
      this.elements.emptyPanel.classList.remove('hide');
    }
    data.forEach(item=>{
      // console.log(counter);
      let session =document.createElement('x-session');
      session.render(item);
      this.elements.root.appendChild(session);

      // counter++;
    })
    // console.log('end setData');

  }

}
window.customElements.define('x-sessions-list', SessionsList);
