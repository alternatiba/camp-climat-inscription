import GenericElement from '../../core/genericElement.js';
import view from './view.html';


import sessionsList from './sessionsList/model.js';
import daysList from './daysList/model.js';
import scrollTop from '../scrollTop/model.js';

export default class Register extends GenericElement {
  constructor() {
    super(view,[]);
    // this.subscribe({
    //   channel: 'main',
    //   topic: 'register',
    //   callback: (data) => {
    //     this.loadComponent(data);
    //   }
    // });
  }

  // loadComponent(comp) {
  //   let screen = this.shadowRoot.querySelector('#screen');
  //   let component = document.createElement(comp);
  //   while (screen.firstChild != null) {
  //     screen.removeChild(screen.firstChild);
  //   }
  //   screen.appendChild(component);
  //
  //   this.propagatedStyle.forEach(style=>{
  //     component.appendChild(style.cloneNode(true));
  //   })
  //   return component;
  // }

  connectedCallback() {
    super.connectedCallback();
  }

  disconnectedCallback() {
    super.disconnectedCallback();
  }

  attributeChangedCallback(attrName, oldVal, newVal) {
    super.attributeChangedCallback(attrName, oldVal, newVal);
  }

}
window.customElements.define('x-register', Register);
