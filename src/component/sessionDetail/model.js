import GenericElement from '../../core/genericElement.js';
import view from './view.html';
import Moment from 'moment'
export default class SessionDetail extends GenericElement {
  constructor() {
    super(view);

    this.elements = {
      userList: this.shadowRoot.getElementById('user-list'),
      row: this.shadowRoot.getElementById('row'),
      title : this.shadowRoot.querySelector('[name="title"]'),
      description : this.shadowRoot.querySelector('[name="description"]'),
      volume : this.shadowRoot.querySelector('[name="volume"]'),
      busyVolume : this.shadowRoot.querySelector('[name="busyVolume"]'),
      date: this.shadowRoot.querySelector('[name="date"]'),
      print: this.shadowRoot.querySelector('#print'),
    };

    this.keySession = null;

    this.subscribe({
      channel: 'session',
      topic: 'loaded',
      callback: (data) => {
        this.setData(data);
      }
    });

    this.subscribe({
      channel: 'pdf',
      topic: 'session.response',
      callback: (data) => {
        this.elements.print.disabled = false;
        if (data.status == 200)
          this.download(data.data);
      }
    });
  }

  connectedCallback() {
    super.connectedCallback();

    this.publish({
      channel: 'session',
      topic: 'loadOne',
      callback: (data) => {
        this.setData(data);
      }
    });

    this.elements.print.addEventListener('click', (e) => {
      this.elements.print.disabled = true;
      this.publish({
        channel: 'pdf',
        topic: 'session.request',
        data: {keySession: this.keySession}
      });
    });
  }

  setData(data) {
    this.elements.busyVolume.textContent=data.busyVolume;
    this.elements.volume.textContent=data.volume;
    this.elements.title.textContent=data.title;
    this.elements.description.textContent=data.abstract;
    this.keySession = data.keySlot;
    this.elements.date.textContent = Moment(data.start).locale('fr').format("dddd D MMMM") +
     ", de " + Moment(data.start).locale('fr').format("HH:mm") + " à " +
     Moment(data.end).locale('fr').format("HH:mm");

    this.elements.print.disabled = false;

    data.users.forEach(user=>{
      let clone = document.importNode(this.elements.row.content, true);

      clone.querySelector('[name="firstname"]').textContent=user.firstname;
      clone.querySelector('[name="lastname"]').textContent=user.lastname;
      clone.querySelector('[name="origin"]').textContent=user.origin;
      clone.querySelector('[name="tel"]').textContent=user.tel;
      clone.querySelector('[name="email"]').textContent=user.email;

      this.elements.userList.appendChild(clone);
    })
  }

  download(data) {
    var blob = new Blob([data], {type: "application/pdf"});
    saveAs(blob, "generated.pdf");
  }
}
window.customElements.define('x-session-detail', SessionDetail);
