import GenericElement from '../../../core/genericElement.js';
import Hamburger from 'elix/src/HamburgerMenuButton.js';
import view from './view.html';

export default class NavigationMenu extends GenericElement {
  constructor() {
    super(view);

    this.elements = {
      menu: this.shadowRoot.querySelector('elix-hamburger-menu-button'),
      logout: this.shadowRoot.querySelector('#logout'),
      volunteerProgress: this.shadowRoot.querySelector('x-progress-bar'),
      averageActivity: this.shadowRoot.querySelector('x-gauge'),
      adminLinks: this.shadowRoot.querySelectorAll('.admin'),
      adminHeader: this.shadowRoot.querySelector('.adminHeader'),
      adminEmail: this.shadowRoot.querySelector('.userEmail'),
      adminBtn: this.shadowRoot.querySelector('.changeUser'),
    }

    this.subscribe({
      channel: 'volunteerPercent',
      topic: 'updated',
      callback: volunteerPercent => {
        this.updateVolunteerProgress(volunteerPercent)
      }
    });

    this.subscribe({
      channel: 'averageActivity',
      topic: 'updated',
      callback: averageActivity => {
        this.updateAverageActivity(averageActivity)
      }
    })

    this.subscribe({
      channel: 'users',
      topic: 'loaded',
      callback: (data) => {
        // console.warn(data);
        this.update(data);
      }
    })

    this.subscribe({
      channel: 'users',
      topic: 'connected.loaded',
      callback: (data) => {
        // console.warn(data);
        this.updateAdmin(data);
      }
    })


  }

  update(data) {
    this.user = data;

    if (this.user.connectedSameSelected) {
      this.elements.adminHeader.classList.add('me');
    } else {
      this.elements.adminHeader.classList.remove('me');
    }
    this.elements.adminEmail.textContent = this.user.email;

  }

  updateAdmin(data) {
    this.userConnected = data;
    // console.warn('userConnected',this.userConnected);
    let isAdmin = this.userConnected.isAdmin==true;
    // console.log(isAdmin);
    if (isAdmin) {
      this.elements.adminHeader.classList.remove('hide');
      this.elements.adminLinks.forEach(element => {
        element.classList.remove('hide');
      });
    }else{
      this.elements.adminHeader.classList.add('hide');
      this.elements.adminLinks.forEach(element => {
        element.classList.add('hide');
      });
    }
  }

  updateVolunteerProgress(volunteerPercent) {
    this.elements.volunteerProgress.setAttribute('value', Math.round(volunteerPercent));
  }

  updateAverageActivity(averageActivity) {
    this.elements.averageActivity.setAttribute('value', averageActivity);
  }


  set selected(comp) {
    this.seleted = comp;
    let selectedElement = this.shadowRoot.querySelector(`#${this.selected}`);
    if (selectedElement != undefined) {
      selectedElement.classList.add('selected');
    }
  }

  connectedCallback() {
    super.connectedCallback();

    this.elements.adminBtn.addEventListener('click', (event) => {
      window.location.href = './#/x-users-list'
    })

    this.publish({
      channel: 'menu',
      topic: 'loadAll'
    });

    this.elements.menu.childNodes.forEach((node) => {
      // console.log('node',node);
      if (node.hasChildNodes() &&
        (
          node.firstChild.nodeName === 'A' ||
          node.firstChild.nodeName === 'BUTTON'
        )
      ) {
        node.firstChild.addEventListener("click", () => {
          // console.log('CLOSE');
          this.elements.menu.close();
        });
      }
    });

    this.elements.logout.addEventListener("click", () => {
      this.publish({
        channel: "auth",
        topic: "logout"
      })
    });
  }

  disconnectedCallback() {
    super.disconnectedCallback();
  }

  attributeChangedCallback(attrName, oldVal, newVal) {
    super.attributeChangedCallback(attrName, oldVal, newVal);
  }

}
window.customElements.define('x-navigation-menu', NavigationMenu);
