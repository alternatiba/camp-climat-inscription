import GenericElement from '../../core/genericElement.js';
import view from './view.html';

import menu from './menu/model.js';
import informations from '../informations/model.js';
import register from '../register/model.js';
import agenda from '../agenda/model.js';
import index from '../index/model.js';
import gauge from '../gauge/model.js';
import progressBar from '../progressBar/model.js';
import registrationGuilde from '../registration-guide/model.js';
import userslist from '../admin/userslist/model.js';
import printer from '../admin/printer/model.js';
import sessionDetail from '../sessionDetail/model.js';
import credits from '../credits/model.js';

let publicPages = ['x-signin', 'x-signup', 'x-index', 'x-forgot-password', 'x-reset-password'];
let adminPages = ['x-users-list', 'x-printer'];

export default class Navigation extends GenericElement {
  constructor() {
    super(view);

    this.elements = {
      screen: this.shadowRoot.querySelector('#screen'),
      menu : this.shadowRoot.querySelector('#menu'),
    }

    this.currentComp=undefined;

    this.subscribe({
      channel: 'main',
      topic: 'screen',
      callback: (data) => {
        this.currentComp=data;
        this.loadComponent();
      }
    });

    // in case jwt is not valid (after 15 days) or no login
    this.subscribe({
      channel: 'auth',
      topic: 'loggedOut',
      callback: () => {
        console.log('loggedOut');
        this.user=undefined;
        window.location.href = './#/x-index';
      }
    });



    // this.updateIfAdmin();

    this.subscribe({
      channel: 'users',
      topic: 'connected.loaded',
      callback: (data) => {
        // console.warn('connected.loaded');
        this.user=data;
        this.loadComponentAuth();
      }
    })

    // this.subscribe({
    //   channel: 'auth',
    //   topic: 'refresh.response',
    //   callback: (data) => {
    //     // console.warn('connected.loaded');
    //     this.loadComponentAuthComponent();
    //   }
    // })

    // this.subscribe({
    //   channel: 'users',
    //   topic: 'loaded',
    //   callback: (data) => {
    //     // console.warn('connected.loaded');
    //     this.user=data;
    //     this.loadComponentAuth();
    //   }
    // })

  }

  updateIfAdmin() {
    //console.log( this.user.isAdmin);
    if(this.user){
      if ( this.user.isAdmin) {
        this.elements.screen.classList.add('admin');
      } else {
        this.elements.screen.classList.remove('admin')
      }
    }

  }

  renderMenu(comp) {
    // console.log('renderMenu');
    // let menuComponent = document.createElement('x-navigation-menu');
    this.updateIfAdmin();
    this.elements.menu.selected = comp;

    // this.elements.screen.appendChild(menuComponent);
  }

  // loadComponent() {
  //
  //   if (this.currentComp!=undefined && this.user!=undefined){
  //     this.loadComponentFinal();
  //     this.updateIfAdmin();
  //   }
  // }

  loadComponentAuth() {
    // console.log('loadComponentAuth',this.user);
    if(this.user!=undefined){
      if (adminPages.includes(this.currentComp)) {
        if(this.user.isAdmin){
          this.loadScreen();
        }else{
          window.location.href = './#/x-registration-guide';
        }
      }else {
        this.loadScreen();
      }
    }else{
      //logout if no tocken send by lowwer layers
      // window.location.href = './#/x-index';
    }
  }


  loadComponent() {
    // console.log('loadComponentFinal',this.user,this.currentComp);


    var authorized = false;
    if (publicPages.includes(this.currentComp)) {
      if(!this.elements.menu.classList.contains('hide')){
          this.elements.menu.classList.add('hide');
      }

      // if(this.user==undefined){
        this.loadScreen();
      // }

    }else{
      if(this.user==undefined){
        //important : this is the main start. it is more than refresh, it is connection with current tocken trigering all
        this.publish({channel: 'auth', topic: 'refresh.request'});
        this.elements.menu.classList.remove('hide');
      }else {
        this.loadComponentAuth();
      }

      // console.log('ALLO',this.user);

    }
  }

  loadScreen(){
    this.elements.screen.innerHTML = ''; // Clean
    let component = document.createElement(this.currentComp);
    this.elements.screen.appendChild(component);
    // console.log(this.elements.screen,component);
    component.classList.add('containerH');
    component.classList.add('grow');
    this.updateIfAdmin();
    this.renderMenu(this.currentComp);
  }

  connectedCallback() {
    super.connectedCallback();

  }

  disconnectedCallback() {
    super.disconnectedCallback();
  }

  attributeChangedCallback(attrName, oldVal, newVal) {
    super.attributeChangedCallback(attrName, oldVal, newVal);
  }

}
window.customElements.define('x-navigation', Navigation);
