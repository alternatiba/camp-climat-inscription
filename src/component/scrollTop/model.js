import GenericElement from '../../core/genericElement.js';
import view from './view.html';

const minScroll = 200;

export default class ScrollTop extends GenericElement {
  constructor() {
    super(view);
    this.elements = {
      button: this.shadowRoot.querySelector('button'),
      scrollTopElement: this.shadowRoot.querySelector('#scrollTop'),
    };
  }

  connectedCallback() {
    super.connectedCallback();
    let assetsContext = require.context('../../assets', false, /\.(png|jpe?g|svg)$/);
    let path = './' + 'top.png';
    let imgData = assetsContext(path);
    this.elements.scrollTopElement.setAttribute('src', imgData);

    window.onscroll = () => {
      if (document.body.scrollTop > minScroll || document.documentElement.scrollTop > minScroll) {
        this.elements.scrollTopElement.classList.add('visible');
      } else {
        this.elements.scrollTopElement.classList.remove('visible');
      }
    };
    this.elements.scrollTopElement.addEventListener('click', () => {
      document.body.scrollTop = 0;
      document.documentElement.scrollTop = 0;
    });
  }
}
window.customElements.define('x-scroll-top', ScrollTop);
