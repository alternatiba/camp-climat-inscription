import GenericElement from '../../../core/genericElement.js';
import view from './view.html';

export default class UsersList extends GenericElement {

  users = [];

  constructor() {
    super(view);

    this.elements = {
      query: this.shadowRoot.querySelector('.queryInput'),
      list: this.shadowRoot.querySelector('.list'),
      row:this.shadowRoot.querySelector('#row'),
    }

    this.subscribe({
        channel: 'users',
        topic: 'search.response',
        callback: (response) => {
          this.users = response.data;
          this.updateList(this.users);
        }
    })
    this.publish({
      channel: 'users',
      topic: 'search.request',
      data: {
        query: ''
      }
  })
  }

  connectedCallback() {
    super.connectedCallback();
    this.elements.query.addEventListener('input', (event) => this.onChange(event));
  }

  updateList(users) {
    // console.log(users)
    this.elements.list.innerHTML = '';
    // var documentFragment = document.createDocumentFragment();
    users.forEach(userData => {

      let clone = document.importNode(this.elements.row.content, true);
      // console.log(clone);
      clone.querySelector('.ticket').textContent=userData.ticketId;
      clone.querySelector('.firstname').textContent=userData.firstname;
      clone.querySelector('.lastname').textContent=userData.lastname;
      clone.querySelector('.email').textContent=userData.email;
      clone.querySelector('.selectButton').data = userData
      // clone.data=userData;
      clone.querySelector('.selectButton').addEventListener('click', (event) => {
        // console.log(event.currentTarget.data);
        this.publish({
            channel: 'users',
            topic: 'select.request',
            data: event.currentTarget.data
        })
        // userService.setUserId(this.userId);
        // userService.setEmail(this.elements.email.textContent);
        window.location.href = './#/x-informations';
        // this.publish({channel: 'user.service', topic: 'reload'});
      });
      this.elements.list.appendChild(clone);


      // let userEl = document.createElement('x-user');
      // userEl.render(userData);
      // this.elements.list.appendChild(userEl);
      // documentFragment.appendChild(userEl);
    })
    // this.elements.list.innerHTML = '';
    // this.elements.list.appendChild(documentFragment);
  }

  onChange(event) {
    this.publish({
        channel: 'users',
        topic: 'search.request',
        data: {
          query: this.elements.query.value
        }
    })
  }
}
window.customElements.define('x-users-list', UsersList);
