import view from './view.html';
import GenericElement from '../../../core/genericElement.js';
import Moment from 'moment';
import { saveAs } from 'file-saver';


export default class Printer extends GenericElement {
  constructor() {
    super(view);

    this.elements = {
      daysloading: this.shadowRoot.querySelector('#daysloading'),
      roomsloading: this.shadowRoot.querySelector('#roomsloading'),
      referentsloading: this.shadowRoot.querySelector('#referentsloading'),
      sessionsloading: this.shadowRoot.querySelector('#sessionsloading'),
      daysoptions: this.shadowRoot.querySelector("#daysoptions"),
      downloadsessions: this.shadowRoot.querySelector("#downloadsessions"),
      downloadrooms: this.shadowRoot.querySelector("#downloadrooms"),
      downloadreferents: this.shadowRoot.querySelector("#downloadreferents"),
    };

    ///////////////////////////////////////
    // Charger les listes déroulantes
    ///////////////////////////////////////

    this.subscribe({
      channel: 'daysWithInfo',
      topic: 'loaded',
      callback: (data) => {
        this.feedDaysOptions(data);
      }
    });

    ///////////////////////////////////////
    // Lancer les téléchargements
    ///////////////////////////////////////

    this.subscribe({
      channel: 'pdf',
      topic: 'volunteer.response',
      callback: (data) => {
        this.elements.sessionsloading.style.display = 'none';
        this.elements.downloadsessions.disabled = false;
        if (data.status == 200)
          this.download(data.data);
      }
    });

    this.subscribe({
      channel: 'pdf',
      topic: 'rooms.response',
      callback: (data) => {
        this.elements.roomsloading.style.display = 'none';
        this.elements.downloadrooms.disabled = false;
        if (data.status == 200)
          this.download(data.data);
      }
    });

    this.subscribe({
      channel: 'pdf',
      topic: 'referents.response',
      callback: (data) => {
        this.elements.referentsloading.style.display = 'none';
        this.elements.downloadreferents.disabled = false;
        if (data.status == 200)
          this.download(data.data);
      }
    });
  }

  connectedCallback() {
    super.connectedCallback();

    this.elements.downloadsessions.addEventListener('click', (e) => {
      var date = this.elements.daysoptions.options[this.elements.daysoptions.selectedIndex].value;
      this.elements.sessionsloading.style.display = 'block';
      this.elements.downloadsessions.disabled = true;
      this.publish({
        channel: 'pdf',
        topic: 'volunteer.request',
        data: {date: date}
      });
    });

    this.elements.downloadrooms.addEventListener('click', (e) => {
      this.elements.roomsloading.style.display = 'block';
      this.elements.downloadrooms.disabled = true;
      this.publish({
        channel: 'pdf',
        topic: 'rooms.request'
      });
    });

    this.elements.downloadreferents.addEventListener('click', (e) => {
      this.elements.referentsloading.style.display = 'block';
      this.elements.downloadreferents.disabled = true;
      this.publish({
        channel: 'pdf',
        topic: 'referents.request'
      });
    });

    this.publish({
      channel: 'days',
      topic: 'loadAll'
    });
  }

  // Remplit la liste déroulante avec les jours
  feedDaysOptions(data) {
    var documentFragment = document.createDocumentFragment();
    data.days.forEach((item, index) => {
      let option = document.createElement('option');
      option.textContent = item.day.format('YYYY-MM-DD');//.format('D MMMM');
      documentFragment.appendChild(option);
    });
    this.elements.daysoptions.innerHTML = '';
    this.elements.daysoptions.appendChild(documentFragment);
    this.elements.daysloading.style.display = 'none';
  }

  download(data) {
    var blob = new Blob([data], {type: "application/pdf"});
    saveAs(blob, "generated.pdf");
  }
}
window.customElements.define('x-printer', Printer);
