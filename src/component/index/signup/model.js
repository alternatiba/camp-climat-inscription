import GenericElement from '../../../core/genericElement.js';
import view from './view.html';
export default class Signup extends GenericElement {
  constructor() {
    super(view);

    this.elements = {
      form: this.shadowRoot.querySelector('#signup'),
      helloasso: this.shadowRoot.querySelector('#helloasso'),
      login: this.shadowRoot.querySelector('#login'),
      password: this.shadowRoot.querySelector('#password'),
      confirm: this.shadowRoot.querySelector('#confirm'),
      error: this.shadowRoot.querySelector('#errors'),
    }

    this.subscribe({
      channel: 'users',
      topic: 'signup.response',
      callback: (data) => {
        if (data.status == 201) {
          window.location.href = './#/x-signin';
        }
        else if (data.status == 409) {
          this.elements.error.textContent = "Cet email est déjà associé à un compte";
        }
        else if (data.status == 403) {
          this.elements.error.textContent = "Cet email n'a pas été trouvé dans la liste des préinscriptions";
        }
        else if (data.status == 499) {
          this.elements.error.textContent = "Un compte est déjà créé pour ce numéro de billet";
        }
        else {
          this.elements.error.textContent = 'Erreur interne';
        }
      }
    })

    this.subscribe({
      channel: 'helloasso',
      topic: 'info.response',
      callback: (response) => {
        console.log(response);
        if (response.status == 200) {
          this.elements.login.value = response.data.email;
        }
        else {
          this.elements.login.value = '';
        }
      }
    })

    const urlParams = new URLSearchParams(window.location.search);
    this.elements.login.value = urlParams.get('login');
    this.elements.helloasso.value = urlParams.get('helloasso');

    this.elements.helloasso.addEventListener("change", (event) => {
      this.publish({
        channel: 'helloasso',
        topic: 'info.request',
        data: {
          helloassoId: this.elements.helloasso.value
        }
      })
    })
  }

  connectedCallback() {
    super.connectedCallback();

    this.elements.form.addEventListener("submit", (event) => {
      event.preventDefault();

      if(this.elements.password.value !== this.elements.confirm.value) {
        this.elements.error.textContent = "Le mot de passe ne correspond pas"
      } else {
        this.publish({channel: 'users', topic: 'signup.request', 'data': {
          helloassoId: this.elements.helloasso.value,
          email: this.elements.login.value,
          password:  this.elements.password.value,
        }});
      }
    });
  }

}
window.customElements.define('x-signup', Signup);
