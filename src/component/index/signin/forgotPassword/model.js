import GenericElement from '../../../../core/genericElement.js';
import view from './view.html';
export default class ForgotPassword extends GenericElement {
  constructor() {
    super(view);

    this.elements = {
      form: this.shadowRoot.querySelector('#forgot'),
      login: this.shadowRoot.querySelector('#login'),
      error: this.shadowRoot.querySelector('#errors'),
      success: this.shadowRoot.querySelector('#success'),
    }

    this.subscribe({
      channel: 'auth',
      topic: 'forgot.response',
      callback: (data) => {
        if (data.status == 200) {
          this.elements.success.textContent = `Un email a été envoyé à votre adresse ${this.elements.login.value}`
        }
        else if (data.status == 404) {
          this.elements.error.textContent = "Cet email n'a pas été trouvé.";
        }
        else {
          this.elements.error.textContent = 'Erreur interne';
        }
      }
    });

    const urlParams = new URLSearchParams(window.location.search);
    this.elements.login.value = urlParams.get('login');
  }

  connectedCallback() {
    super.connectedCallback();

    this.elements.form.addEventListener("submit", (event) => {
      event.preventDefault();

      this.publish({channel: 'auth', topic: 'forgot.request', 'data': {
        email: this.elements.login.value,
      }});
    });
  }

}
window.customElements.define('x-forgot-password', ForgotPassword);
