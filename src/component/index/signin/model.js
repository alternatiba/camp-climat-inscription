import GenericElement from '../../../core/genericElement.js';
import view from './view.html';
import forgotPassword from './forgotPassword/model.js';
import resetPassword from './resetPassword/model.js';

export default class Signin extends GenericElement {
  constructor() {
    super(view);

    this.elements = {
      form: this.shadowRoot.querySelector('#signin'),
      login: this.shadowRoot.querySelector('#login'),
      password: this.shadowRoot.querySelector('#password'),
      error: this.shadowRoot.querySelector('#errors'),
    }

    this.subscribe({
      channel: 'auth',
      topic: 'login.response',
      callback: (data) => {
        console.log('login.response',data);
        if (data.status == 200) {
          this.publish({channel: 'auth', topic: 'connected'});
          window.location.href = './#/x-registration-guide';
          // try {
          //   var cred = new PasswordCredential(this.elements.form);
          //   navigator.credentials.store(cred)
          //   .then(function() {
          //     window.location.href = './#/x-register';
          //   });
          // } catch(e) {
          //   window.location.href = './#/x-register';
          // }
        }
        else {
          this.elements.error.textContent = 'Email ou mot de passe incorrect.'
        }
      }
    });

    const urlParams = new URLSearchParams(window.location.search);
    this.elements.login.value = urlParams.get('login');
    console.log('WTF');
  }

  connectedCallback() {
    super.connectedCallback();

    this.elements.form.addEventListener("submit", (event) => {
      event.preventDefault();
      this.publish({channel: 'auth', topic: 'login.request', 'data': {
        email: this.elements.login.value,
        password:  this.elements.password.value,
      }});
    });
  }

}
window.customElements.define('x-signin', Signin);
