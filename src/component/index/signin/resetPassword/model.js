import GenericElement from '../../../../core/genericElement.js';
import view from './view.html';
export default class ResetPassword extends GenericElement {
  constructor() {
    super(view);

    this.elements = {
      form: this.shadowRoot.querySelector('#reset'),
      password: this.shadowRoot.querySelector('#password'),
      confirm: this.shadowRoot.querySelector('#confirm'),
      error: this.shadowRoot.querySelector('#errors'),
      success: this.shadowRoot.querySelector('#success'),
      signinLink: this.shadowRoot.querySelector('#signin-link'),
    }

    this.subscribe({
      channel: 'auth',
      topic: 'reset.response',
      callback: (data) => {
        if (data.status == 200) {
          this.elements.success.textContent = 'Le mot de passe a été réinitalisé.'
          this.elements.signinLink.style = 'display:block';
        } else {
          this.elements.error.textContent = 'Erreur interne';
        }
      }
    })
  }

  connectedCallback() {
    super.connectedCallback();

    this.elements.form.addEventListener("submit", (event) => {
      event.preventDefault();

      if(this.elements.password.value !== this.elements.confirm.value) {
        this.elements.error.textContent = "Le mot de passe ne correspond pas"
      } else {
        this.publish({channel: 'auth', topic: 'reset.request', 'data': {
          password:  this.elements.password.value,
          token: this.getToken(),
        }});
      }
    });
  }

  getToken() {
    let regex = /\#\/x-reset-password\?.*token=(.+)\/?/ig;
    let regExec = regex.exec(document.location.hash);
    if (regExec) {
      return regExec[1];
    }
    return null;
  }

}
window.customElements.define('x-reset-password', ResetPassword);
