import GenericElement from '../../core/genericElement.js';
import view from './view.html';
import signin from './signin/model.js';
import signup from './signup/model.js';

export default class Index extends GenericElement {
  constructor() {
    super(view);
  }

  connectedCallback() {
    super.connectedCallback();
  }

}
window.customElements.define('x-index', Index);
