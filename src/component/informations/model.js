import Moment from 'moment'
import GenericElement from '../../core/genericElement.js';
import view from './view.html';

export default class Informations extends GenericElement {
  constructor() {
    super(view);

    this.elements = {
      form: this.shadowRoot.querySelector('#form'),
      arrivalDate: this.shadowRoot.querySelector('#arrivalDate'),
      departureDate: this.shadowRoot.querySelector('#departureDate'),
      arrivalHour: this.shadowRoot.querySelector('#arrivalHour'),
      departureHour: this.shadowRoot.querySelector('#departureHour'),
      arrivalError: this.shadowRoot.querySelector('#arrival-error'),
      departureError: this.shadowRoot.querySelector('#departure-error'),
      comment: this.shadowRoot.querySelector('#comment'),
      phone: this.shadowRoot.querySelector('#phone'),
      firstname: this.shadowRoot.querySelector('#firstname'),
      lastname: this.shadowRoot.querySelector('#lastname'),
      origin: this.shadowRoot.querySelector('#origin'),
      wantPrint: this.shadowRoot.querySelector('#wantPrint'),
      curriculumTemplate: this.shadowRoot.querySelector('#curriculumTemplate'),
      curriculumsContainer : this.shadowRoot.querySelector('#curriculumsContainer'),
      submit : this.shadowRoot.querySelector('#submit'),
      paid: this.shadowRoot.querySelector('#paid'),
      paidContainer: this.shadowRoot.querySelector('#paidContainer'),
    };

    this.subscribe({
      channel: 'users',
      topic: 'changed',
      callback: (data) => {
        // console.warn('changed');
        this.setFormData(data);
      },
    });

    this.subscribe({
      channel: 'users',
      topic: 'update.response',
      callback: (data) => {
        this.setFormData(data.data);
      },
    });

    this.subscribe({
      channel: 'users',
      topic: 'changed.connected',
      callback: (data) => {
        this.setConnected(data)
      }
    });

    this.subscribe({
      channel: 'curriculums',
      topic: 'loaded',
      callback: (data) => {
        // console.warn('Curriculums',data);
        if (data!=undefined){
          this.curriculums=data;
          let curriculumTemplate=this.elements.curriculumTemplate;
          let assetsContext = require.context('../../assets', false, /\.(png|jpe?g|svg)$/);
          data.forEach(curiculum=>{
            let clone = document.importNode(curriculumTemplate.content, true);
            clone.querySelector('[name="curriculumLabel"]').appendChild(document.createTextNode(curiculum.desc));
            clone.querySelector('[name="curriculumCheck"]').value=curiculum.key;
            let path = './' + curiculum.asset;
            let imgData = assetsContext(path);
            let img = document.createElement('img')
            img.setAttribute('src', imgData);
            let style =`border: 3px solid ${curiculum.color}`
            img.style = style;
            clone.querySelector('[name="asset"]').appendChild(img);

            // if(this.user.curriculums.includes(curiculum.key)){
            //   clone.querySelector('[name="curriculumCheck"]').checked=true;
            // }
            // clone.querySelector('[name="curriculumCheck"]').addEventListener('change',this.changeCurriculum.bind(this));
            Array.from(clone.childNodes).forEach(c=>{
                this.elements.curriculumsContainer.appendChild(c);
            })
          })
          this.checkCurriculums();
        }
      },
    });
  }

  mixConnectedAndUser(){
    if(!this.userConnected.isAdmin && this.user.paid==true){
      this.elements.arrivalDate.setAttribute('disabled',true);
      this.elements.departureDate.setAttribute('disabled',true);
      this.elements.arrivalHour.setAttribute('disabled',true);
      this.elements.departureHour.setAttribute('disabled',true);
    }
  }

  setConnected(data) {
    this.userConnected=data;
    if(data.isAdmin){
      this.elements.paidContainer.classList.remove('hide');
    }
    if(this.user!=undefined){
      this.mixConnectedAndUser();
    }
  }

  setFormData(data) {
    this.user=data;
    var formatDate = (d) => {
      var date = Moment(d).utcOffset(120);
      return date.format('YYYY-MM-DD');
    };
    var formatHour = (d) => {
      var date = Moment(d).utcOffset(120);
      return date.format('HH:mm');
    };
    this.elements.arrivalDate.value = formatDate(data.arrivalDate);
    this.elements.departureDate.value = formatDate(data.leavingDate);
    this.elements.arrivalHour.value = formatHour(data.arrivalDate);
    this.elements.departureHour.value = formatHour(data.leavingDate);
    this.elements.comment.value = data.comment || "";
    this.elements.phone.value = data.tel || "";
    this.elements.firstname.value = data.firstname || "";
    this.elements.lastname.value = data.lastname || "";
    this.elements.origin.value = data.origin || "";
    this.elements.wantPrint.checked = data.wantPrint || false;
    this.elements.paid.checked = data.paid || false;
    this.checkCurriculums();
    if(this.userConnected!=undefined){
      this.mixConnectedAndUser();
    }

  }

  checkCurriculums(){
    if (this.user!=undefined && this.curriculums!=undefined){
      this.user.curriculums.forEach(c=>{
        this.elements.curriculumsContainer.querySelector('input[value="'+c+'"]').checked=true;
      });
    }
  }

  connectedCallback() {
    super.connectedCallback();

    this.publish({channel: "users", topic: "get", data: {}});
    this.publish({channel: "curriculums", topic: "loadAll", data: {}});
    this.publish({
      channel: 'users',
      topic: 'get.connected'
    })
    this.elements.form.addEventListener("submit", (event) => {
      event.preventDefault();
      if(this.checkValidity()) {
        this.publish({channel: 'users', topic: 'update.request', data: {
          arrivalDate: this.getArrival(),
          leavingDate: this.getDeparture(),
          comment: this.getComment(),
          phoneNumber: this.getPhone(),
          firstname: this.getFirstname(),
          lastname: this.getLastname(),
          origin: this.getOrigin(),
          wantPrint: this.getWantPrint(),
          curriculums :this.getCurriculums(),
          paid :this.getPaid(),
        }});
        window.location.href = './#/x-register?day=1'
      }
    });

    this.elements.arrivalDate.addEventListener("blur", (event) => {this.checkValidity()});
    this.elements.departureDate.addEventListener("blur", (event) => {this.checkValidity()});
    this.elements.arrivalDate.addEventListener("input", (event) => {this.checkValidity()});
    this.elements.departureDate.addEventListener("input", (event) => {this.checkValidity()});
    this.elements.arrivalHour.addEventListener("blur", (event) => {this.checkValidity()});
    this.elements.departureHour.addEventListener("blur", (event) => {this.checkValidity()});
    this.elements.arrivalHour.addEventListener("input", (event) => {this.checkValidity()});
    this.elements.departureHour.addEventListener("input", (event) => {this.checkValidity()});
  }

  getArrival() {
    var date =  Moment.parseZone(`${this.elements.arrivalDate.value}T${this.elements.arrivalHour.value}+02:00`);
    if (date.isValid()) {
      return date;
    } else {
      return null;
    }
  }

  getDeparture() {
    var date = Moment.parseZone(`${this.elements.departureDate.value}T${this.elements.departureHour.value}+02:00`);
    if (date.isValid()) {
      return date;
    } else {
      return null;
    }
  }

  getComment() {
    return this.elements.comment.value;
  }

  getPhone() {
    return this.elements.phone.value;
  }

  getFirstname() {
    return this.elements.firstname.value;
  }

  getLastname() {
    return this.elements.lastname.value;
  }

  getOrigin() {
    return this.elements.origin.value;
  }

  getWantPrint() {
    return this.elements.wantPrint.checked;
  }

  getCurriculums() {
    return Array.from(this.elements.curriculumsContainer.querySelectorAll('[name="curriculumCheck"]')).filter(cb=>cb.checked==true).map(cb=>cb.value);
  }

  getPaid() {
    console.log('getPaid',this.elements.paid.checked);
    return this.elements.paid.checked;
  }

  // changeCurriculum(e){
  //   // let checkBoxes = this.elements.curriculumsContainer.querySelectorAll('[name="curriculumCheck"]');
  //   let checked=Array.from(this.elements.curriculumsContainer.querySelectorAll('[name="curriculumCheck"]')).filter(cb=>cb.checked==true).map(cb=>cb.value);
  //   console.log(checked);
  //
  // }

  checkValidity() {
    var formValidity = this.elements.form.checkValidity();
    var arrivalValidity = false;
    var departureValidity = false;

    var arrival = this.getArrival();
    var departure = this.getDeparture();

    if(arrival) {
      arrivalValidity = true;
    } else {
      this.elements.arrivalError.textContent = "!"
    }

    if (departure) {
      if(arrival && departure.isBefore(arrival)) {
        var message = `Tu ferais mieux d'arriver avant de partir`;
        this.elements.departureDate.setCustomValidity(message);
        this.elements.departureError.textContent = message;
      } else {
        this.elements.departureDate.setCustomValidity("");
        this.elements.departureError.textContent = "";
        departureValidity = true;
      }
    } else {
      this.elements.departureError.textContent = "!"
    }

    return formValidity && arrivalValidity && departureValidity;
  }

}
window.customElements.define('x-informations', Informations);
