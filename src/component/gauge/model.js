import { scale } from 'chroma-js';
import GenericElement from '../../core/genericElement.js';
import view from './view.html';

export default class Gauge extends GenericElement {
  static get observedAttributes() {
    return ['value'];
  }

  constructor() {
    super(view);

    this.elements = {
      gaugeContainer: this.shadowRoot.querySelector('#gauge-container'),
      gaugeValue: this.shadowRoot.querySelector('#gauge-value'),
    };

    this.min = this.getAttribute('min');
    this.max = this.getAttribute('max');
    this.middle = (this.max - this.min) / 2;
    this.setValue(this.getAttribute('value'));
  }

  setValue(value) {
    value = Number(value);
    if (value < this.min) {
      this.value = this.min;
    } else if (value > this.max) {
      this.value = this.max;
    } else {
      this.value = value;
    }
  }

  updateUI() {
    const isNegativeValue = this.value < this.middle;
    const ratio = Math.abs((this.value - this.middle) / (this.max - this.min) * 2);
    const percentWidth = `${ratio * 50}%`;

    const positiveColorScale = scale(['#087f49', 'yellow', 'red']);
    const negativeColorScale = scale(['#087f49', 'yellow', 'red']);

    this.elements.gaugeValue.style.width = percentWidth;
    if (isNegativeValue) {
      this.elements.gaugeValue.style.transform = 'translateX(-100%)';
      this.elements.gaugeValue.style.backgroundColor = negativeColorScale(ratio);
    } else {
      this.elements.gaugeValue.style.transform = 'translateX(0%)';
      this.elements.gaugeValue.style.backgroundColor = positiveColorScale(ratio);
    }
  }

  attributeChangedCallback(attrName, oldVal, newVal) {
    if (attrName === 'value') {
      this.setValue(newVal);
      this.updateUI();
    }
  }

  connectedCallback() {
    super.connectedCallback();

    this.updateUI();
  }
}
window.customElements.define('x-gauge', Gauge);
