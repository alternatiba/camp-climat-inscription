import GenericElement from '../../../core/genericElement.js';
import view from './view.html';
export default class AgendaLine extends GenericElement {
  constructor() {
    super(view);

    this.elements = {
      container: this.shadowRoot.querySelector('.container'),
      day: this.shadowRoot.querySelector('.day'),
      time: this.shadowRoot.querySelector('.time'),
      location: this.shadowRoot.querySelector('.location'),
      title: this.shadowRoot.querySelector('.title'),
      assetDomain:  this.shadowRoot.querySelector('.assetDomain'),
      abstract: this.shadowRoot.querySelector('.abstract'),
      role: this.shadowRoot.querySelector('.role'),
      keySlot: this.shadowRoot.querySelector('.keySlot'),
      info: this.shadowRoot.querySelector('.info'),
    }

    this.assetsContext = require.context('../../../assets', false, /\.(png|jpe?g|svg)$/);
  }

  attributeChangedCallback(attrName, oldVal, newVal) {
    this.render();
  }

  connectedCallback() {
    super.connectedCallback();
    // this.render();
  }

  render(data) {
    if (data.noDay!==true) {
      this.elements.day.innerHTML = data.day.format('dddd Do');
      this.elements.container.classList.add('newday');
    } else {
      this.elements.day.classList.add('empty');
    }

    let asset = data.assetDomain;
    if (asset != undefined) {
      let path = './' + asset;
      let imgData = this.assetsContext(path);
      let img = document.createElement('img')
      img.setAttribute('src', imgData);
      this.elements.assetDomain.appendChild(img);
    }
    // this.elements.assetDomain.innerHTML = this.getAttribute('assetDomain') || "";
    this.elements.time.innerHTML = `${data.start.format('HH:mm')} - ${data.end.format('HH:mm')}`;
    this.elements.role.innerHTML = data.isTrainer?data.refererLabel:'participant.e';
    this.elements.location.innerHTML = data.location ;
    this.elements.title.innerHTML = data.title ;
    this.elements.abstract.innerHTML = data.important!=undefined?data.important.replace(/(?:\r\n|\r|\n)/g, '<br/>') : "";
    this.elements.keySlot.innerHTML = data.keySlot;
    if(data.isTrainer){
      this.elements.info.setAttribute('href', this.elements.info.getAttribute('href-base') + data.keySlot);
      this.elements.info.classList.remove('hide');
      if(!this.elements.info.firstChild){
        let imgData = this.assetsContext('./list.png');
        // this.elements.info.setAttribute('href', this.elements.info.getAttribute('href') + data.keySlot);
        // let imgContainer = document.createElement('div');
        let img = document.createElement('img');
        img.setAttribute('src', imgData);
        this.elements.info.appendChild(img);
      }
    }

  }

}

window.customElements.define('x-agenda-ligne', AgendaLine);
