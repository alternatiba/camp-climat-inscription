import GenericElement from '../../core/genericElement.js';
import view from './view.html';
import AgendaLine from './agendaLine/model.js';

export default class Agenda extends GenericElement {
  constructor() {
    super(view);

    this.elements = {
      container: this.shadowRoot.querySelector('#sessions'),
      print: this.shadowRoot.querySelector('#print'),
    };

    this.subscribe({channel: 'agenda', topic: 'updated', callback: sessions => {
        this.render(sessions);
      }
    });

    this.subscribe({channel: 'pdf', topic: 'planning.response', callback: data => {
        this.elements.print.disabled = false;
        if (data.status == 200)
          this.download(data.data);
      }
    });
  }

  render(sessions) {
    var documentFragment = document.createDocumentFragment();

    var previousDay = null;
    sessions.forEach((session) => {
      var line = document.createElement('x-agenda-ligne');

      if (session.day.isSame(previousDay) === true) {
        // previousDay = session.day;
        // line.setAttribute('day', session.day.format('dddd Do'));
        session.noDay=true;
      }else{

      }
        previousDay = session.day;

      line.render(session);

      // if (session.location) { line.setAttribute('location', session.location); }
      // if (session.refererLabel) { line.setAttribute('role', session.isTrainer?session.refererLabel:'participant.e'); }
      // if (session.assetDomain) { line.setAttribute('assetDomain', session.assetDomain); }
      // if (session.title) { line.setAttribute('title', session.title); }
      // if (session.keySlot) { line.setAttribute('keySlot', session.keySlot); }
      // line.setAttribute('time', `${session.start.format('HH:mm')} - ${session.end.format('HH:mm')}`);
      // line.setAttribute('title', session.title || " ");
      // var text = '';
      // if (session.important) {
      //   text = session.important.replace(/(?:\r\n|\r|\n)/g, '<br/>');
      // }
      // line.setAttribute('abstract', text);

      documentFragment.appendChild(line);
    });
    this.elements.container.innerHTML = "";
    this.elements.container.appendChild(documentFragment);
  }

  connectedCallback() {
    super.connectedCallback();
    this.publish({channel: 'agenda', topic: 'loadAll'});

    this.elements.print.addEventListener('click', (e) => {
      this.elements.print.disabled = true;
      this.publish({
        channel: 'pdf',
        topic: 'planning.request',
        data: {
          userId: null,
        }
      });
    });
  }

  disconnectedCallback() {
    super.disconnectedCallback();
  }

  download(data) {
    var blob = new Blob([data], {type: "application/pdf"});
    saveAs(blob, "Mon_Planning_Camp_Climat.pdf");
  }
}

window.customElements.define('x-agenda', Agenda);
