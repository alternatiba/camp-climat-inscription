import Moment from 'moment';

export default class Session {
  constructor(sessionData) {
    this.id = sessionData.id;
    this.start = Moment(sessionData.start).locale('fr').utcOffset(120);
    this.end = Moment(sessionData.end).locale('fr').utcOffset(120);
    this.location = sessionData.descRoom;
    this.title = sessionData.title;
    this.important = sessionData.important;
    this.abstract = sessionData.description;
    this.isFull = sessionData.isFull;
    this.busyVolume = sessionData.busyVolume || 0;
    this.volume = sessionData.volume || 0;
    // this.maxSlots = sessionData.volume || sessionData.maxSlots;
    this.dependsOn = sessionData.dependsOn;
    this.linkedWith = sessionData.linkedWith;
    this.trainers = sessionData.trainers ||  sessionData.trainer;
    this.domain = sessionData.domain || '';
    // this.isVolunteer = sessionData.isVolunteer || sessionData.volunteerFactor > 0;
    this.volunteerFactor = sessionData.volunteerFactor;
    this.keyTraining = sessionData.keyTraining;
    this.keySlot = sessionData.keySlot;
    // this.noRegistration = sessionData.isInscriptionFree || false;
    this.registered = false;
    this.isTrainer = false;
    this.colorDomain = sessionData.colorDomain;
    this.descDomain = sessionData.descDomain;
    this.assetDomain = sessionData.assetDomain;
    this.isSecret = sessionData.isSecret;
    this.isInscriptionFree = sessionData.isInscriptionFree;
    this.curriculums=sessionData.curriculums;
  }

  get duration() {
    var diff = this.end.diff(this.start, 'hours');
    if (diff > 0) {
      return diff;
    } else {
      // console.log(`Duration = ${diff}`, this.keySlot);
      return 0;
    }
  }

  get refererLabel() {
    if(this.domain=='ENF'){
      return "Animateur.trice"
    } else if (this.domain=='DET'){
      return "Intervenant.e"
    } else if (this.domain=='BENEVOL'){
      return "Référent.e"
    } else {
      return "Formateur.trice"
    }
  }

  get day() {
    return this.start.clone().startOf('day');
  }

  get freeSlots() {

      return this.volume - this.busyVolume;

  }

  isConflictingWith(otherSession) {
    if (otherSession.keySlot === this.keySlot) {
      return false;
    }
    return ((otherSession.isInscriptionFree!=true && this.isInscriptionFree!=true) && !(otherSession.end <= this.start || this.end <= otherSession.start));
  }

  hasTrainer(ticketNumber) {
    return ticketNumber &&
      this.trainers &&
      this.trainers.filter( t => `${t.ticketId}` === `${ticketNumber}`).length > 0;
  }

  isVisibleFor(ticketNumber) {
    if(this.isHiddenSession()) {
      return this.hasTrainer(ticketNumber);
    } else {
      return true;
    }
  }

  isHiddenSession() {
    return this.isSecret;
  }
}
