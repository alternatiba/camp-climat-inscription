import Session from './session';
import Moment from 'moment';


class Store {
  VOLUNTEER_POINT_BY_DAY = 2.5;
  days = [];
  sessions;
  user;
  arrival;
  departure;
  //userTicket = "ABCD123";
  // userTicket = null;
  volunteerScore = 0;
  volunteerPercent = 0;
  averageActivity = 0;
  startDayVolunteer = Moment('2019-07-31');
  endDayVolunteer = Moment('2019-08-11');

  setSessions(sessions) {
    //console.log('input', sessions.map((s) => s.start).sort())
    // console.log(sessions);
    this.sessions = sessions.map(session => new Session(session)).sort((s1, s2) => {
      return s1.start.valueOf() - s2.start.valueOf();
    });

    let allDays = this.sessions.map(s => {
      return s.day
    });
    let distinctDays = allDays.reduce((acc, d) => {
      if (acc.length === 0) {
        return [d];
      } else if (d.valueOf() === acc[acc.length - 1].valueOf()) {
        return acc;
      } else {
        acc.push(d);
        return acc;
      }
    }, []);

    this.days = distinctDays;
    //console.log('distinctDays', distinctDays.length, distinctDays.map(x => x.format()));
    this.mixUserAndSession();
    // if (this.registrations) {
    //   this.applyRegistrationToSessions();
    //
    // }
    // if (this.curriculums) {
    //   // this.applyUserCurriculumsToSessions();
    // }

    this.printSessionsReport(sessions);
  }

  setUserWithoutImpact(user) {
    this.setUser(user, true);
  }

  selectSession(keySlot){
    console.log('selectSession',keySlot);
    this.selectedKeySlot=keySlot;

  }

  applySelectedKeySlot(){
    this.selectedSession=this.sessions.filter(s=>s.keySlot==this.selectedKeySlot)[0];
    console.log('applySelectedKeySlot',this.selectedSession);
  }

  // get connectedSameSelected(){
  //   return this.user.user.ticketId==this.connectedUser.ticketId;
  // }

  setUser(user, noImpact) {
    // console.log('setUser',user);
    this.user = user;
    this.userTicket = user.ticketId;
    if (this.connectedUserId == user.id) {
      this.connectedUser = user;
      user.connectedSameSelected = true;
    } else {
      user.connectedSameSelected = false;
    }
    if (noImpact != true) {
      this.mixUserAndSession();
    }

    // this.setRegistrations(user.sessions);
    // this.setUserCurriculums(user.curriculums);
  }

  setCurriculums(curriculums) {
    this.curriculums = curriculums;
  }

  // setRegistrations(sessionKeys) {
  //   this.registrations = sessionKeys;
  //   if (this.isEmpty() === false) {
  //     this.applyRegistrationToSessions();
  //   }
  // }
  // setUserCurriculums(curriculums) {
  //   // console.log('setUserCurriculums');
  //   this.userCurriculums = curriculums;
  //   // console.log(this.isEmpty());
  //   if (this.isEmpty() === false) {
  //     this.applyUserCurriculumsToSessions();
  //   }
  // }

  // updatePresence(pArrival, pDeparture, ticketId) {
  //   var arrival = Moment(pArrival).locale('fr').utcOffset(120);
  //   var departure = Moment(pDeparture).locale('fr').utcOffset(120);
  //   const outOfPrensenceRegistrations = this.store.setPrensenceDays(arrival, departure, ticketId);
  //   if (outOfPrensenceRegistrations) {
  //     outOfPrensenceRegistrations.forEach(session => this.unregister(session.keySlot));
  //   }
  //   this.loadDays();
  //   this.loadSessions();
  //   this.publishUserProgression();
  //   this.publishAgenda();
  // }

  mixUserAndSession() {
    if (this.user != undefined && this.sessions != undefined) {
      this.applyUserToSession();
    }
  }

  applyUserToSession() {
    // console.warn('applyUserToSession');
    // console.trace();
    this.applyRegistrationToSessions();
    this.applyUserCurriculumsToSessions();
    this.applyPrensenceDays();
  }

  applyRegistrationToSessions() {
    // console.log('applyRegistrationToSessions', this.user);
    this.getTrainerSessions().forEach(s => {
      s.isTrainer = true;
      this.sessionRegistred(s.keySlot);
    });
    this.user.sessions.forEach(key => {
      this.sessionRegistred(key);
    });
  }

  applyUserCurriculumsToSessions() {
    // console.log("applyUserCurriculumsToSessions",this.userCurriculums);
    this.sessions.forEach(session => {
      session.curriculums.forEach(c => {
        if (this.user.curriculums.includes(c.key)) {
          c.isCuriculumUser = true;
        } else {
          c.isCuriculumUser = false;
        }
      })
    });
  }

  applyPrensenceDays() {
    // console.log('Set presence', this.user.arrivalDate.format(), this.user.leavingDate.format());
    var arrival = Moment(this.user.arrivalDate).locale('fr').utcOffset(120);
    var departure = Moment(this.user.leavingDate).locale('fr').utcOffset(120);
    // console.log('Set presence', arrival.format(), departure.format());
    // this.setPrensenceDays(arrival,departure,this.user.ticketId);
    this.arrival = arrival;
    this.departure = departure;
    // if (userTicket) {
    //   this.userTicket = userTicket;
    // }

    // return this.registeredSessions.filter(s => {
    //   return s.day.isBetween(arrival, departure, null, '[]') == false;
    // });
  }

  getTrainerSessions() {
    if(this.sessions!=undefined){
      return this.sessions.filter(s => {
        return s.hasTrainer(this.userTicket);
      });
    }else{
      return [];
    }

  }

  isEmpty() {
    // console.log(this.days);
    return this.days.length === 0;
  }
  cleanSession() {
    this.sessions = undefined;
    this.days = [];
  }

  cleanUser() {
    this.user = undefined;
    this.connectedUser = undefined;
    this.connectedSameSelected = undefined;
  }

  cleanAll() {
    this.cleanSession();
    this.cleanUser();
  }

  get volunteerDays() {
    let startWork = this.startDayVolunteer.clone().add(1, 'days');
    let endWork = this.endDayVolunteer.clone().subtract(1, 'days');
    return this.presenceDays.filter((d) => {
      let current = Moment(d);
      let isBetween = current.isBetween(startWork, endWork, null, '[]');
      // console.log(d.format('YYYY-MM-DD'),startWork.format('YYYY-MM-DD'),endWork.format('YYYY-MM-DD'),isBetween);
      //console.log('isBetween', isBetween, d.format());
      return isBetween;

    });
  }

  get presenceDays() {

    return this.days.filter((d) => {
      if (this.departure && this.arrival) {
        let isBetween = d.isBetween(this.arrival, this.departure, 'day', '[]');
        //console.log('isBetween', isBetween, d.format());
        return isBetween;
      } else {
        return true;
      }
    });
  }

  getSessionById(sessionId) {
    return this.sessions.find(s => s.id === sessionId);
  }

  getSessionByKey(sessionKey) {
    return this.sessions.find(s => s.keySlot === sessionKey);
  }

  getSessionsByDate(date) {
    console.log('getSessionsByDate',this.arrival, this.departure);
    // console.trace();
    return this.sessions.filter(s => {
      return s.day.isSame(date) &&
        (s.isVisibleFor(this.userTicket) ||
          this.connectedUser.isAdmin == true ||
          s.registered == true) &&
        s.start.isAfter(this.arrival) && s.end.isBefore(this.departure);
    });
  }

  getSessionsByDayIndex(dayIndex) {
    if (dayIndex) {
      let day = this.presenceDays[dayIndex - 1];
      if (day) {
        return this.getSessionsByDate(day);
      }
    }
    return [];
  }

  get registeredSessions() {
    if (this.sessions != undefined) {
      return this.sessions.filter(s => s.registered);
    } else {
      return [];
    }

  }
  //
  // getVolunteerSessions() {
  //   if (this.user != undefined) {
  //     // console.log('getVolunteerSessions',this.user.ticketId);
  //     return this.registeredSessions
  //       .filter(session => session.registered);
  //   } else {
  //     return [];
  //   }
  // }

  getVolunteerPercent() {

    // console.log('getVolunteerSessions',this.registeredSessions);
    const volunteerScore = this.registeredSessions
      .reduce((score, curSession) => {
        let curScore= curSession.hasTrainer(this.userTicket)?curSession.duration:curSession.duration * curSession.volunteerFactor
        return score + curScore;
      }, 0);
    // console.log('getTrainerSessions',this.getTrainerSessions());
    // const trainerScore = this.getTrainerSessions()
    //   .reduce((score, curSession) => {
    //     return score + (curSession.duration);
    //   }, 0);
    // let totalScore = volunteerScore+trainerScore;
    const nbDay = this.volunteerDays.length;
    // console.log('nbDay',nbDay);
    this.volunteerPercent = volunteerScore / (nbDay * this.VOLUNTEER_POINT_BY_DAY) * 100;

    return this.volunteerPercent;
  }

  getAverageActivity() {
    // return value between 0 et 10
    const averageActivity = this.registeredSessions
      .reduce((totalDuration, curSession) => {
        return totalDuration + curSession.duration;
      }, 0) / this.presenceDays.length;
    this.averageActivity = averageActivity;
    return this.averageActivity;
  }

  sessionRegistred(sessionKey) {
    // console.warn("sessionRegistred",sessionKey)
    var session = this.getSessionByKey(sessionKey);

    if (session) {
      session.registered = true;

      // if (session.isHiddenSession()) {
      //   // No conflict
      //   return [session];
      // } else {
      var conflictings = this.getSessionsInConflict(session);
      conflictings.forEach((s) => s.conflicting = session);
      return [session].concat(conflictings);
      // }
    } else {
      console.error(`Session id not found : ${sessionKey}. that could be happend because ObjectId change betwen inscription and new data`);
      return [];
    }
  }

  sessionUnregistred(sessionKey) {
    let session = this.getSessionByKey(sessionKey);
    session.registered = false;

    let conflictings = this.getSessionsInConflict(session);
    conflictings.forEach((s) => {
      let secondLevelConflicts = this.getSessionsInConflict(s, true)
      if (secondLevelConflicts.length == 0) {
        s.conflicting = null
      }
    });

    return [session].concat(conflictings);
  }

  getLinkedSessions(session) {
    let result = session.keySlot.match(/(.*\[\d+\])\[\d+\]/);
    if (!result || result.length !== 2) {
      return [];
    }
    let regex = new RegExp(('^' + result[1]).replace('[', '\\[').replace(']', '\\]'));
    return this.sessions.filter(sess => {
      return sess.keySlot.match(regex) ? true : false;
    });
  }

  getSessionsInConflict(session, registredOrTrainer) {
    registredOrTrainer = registredOrTrainer == undefined ? false : registredOrTrainer;
    var linkedSessions = this.getLinkedSessions(session);
    var conflictings = [];
    linkedSessions.forEach(linkedSession => {
      let inspectSessions;
      if (registredOrTrainer == true) {

        inspectSessions = this.sessions.filter(s =>
          s.registered == true || s.hasTrainer(this.userTicket)
        );
      } else {
        inspectSessions = this.sessions;
      }
      inspectSessions.forEach((otherSession) => {
        if (conflictings.findIndex(sess => (sess.keySlot === otherSession.keySlot)) !== -1) {
          return;
        }
        if (linkedSessions.findIndex(sess => (sess.keySlot === otherSession.keySlot)) !== -1) {
          return;
        }

        if (linkedSession.isConflictingWith(otherSession)) {
          conflictings.push(otherSession);
          var linkedOtherSession = this.getLinkedSessions(otherSession);
          linkedOtherSession.forEach(s => {
            if (conflictings.findIndex(sess => (sess.keySlot === s.keySlot)) !== -1) {
              return;
            }
            if (linkedSessions.findIndex(sess => (sess.keySlot === s.keySlot)) !== -1) {
              return;
            }
            if (linkedSession.isConflictingWith(otherSession)) {
              conflictings.push(s);
            }
          });
        }
      });
    })
    return conflictings;
  }

  isUnregistrationForbidden(sessionKey) {
    var campStart = Moment('2019-07-31T00:00:00.000+02:00').locale('fr');
    var now = Moment();
    if (now.isAfter(campStart)) {
      var session = this.getSessionByKey(sessionKey);
      if (session) {
        if (session.isVolunteer) {
          return "Il est trop tard pour se désinscrire du bénévolat"
        } else {
          return false;
        }
      } else {
        return "Erreur interne, session inconnu";
      }
    } else {
      return false;
    }
  }

  printSessionsReport(sessions) {
    //console.log('Sessions with trainers', sessions.filter(s => s.trainers.length>0));
    // console.log('Sessions without registration', sessions.filter(s => s.isInscriptionFree));
    //console.log('Secret sessions', sessions.filter(s => s.isSecret));
    // console.log('Volonteering sessions', sessions.filter(s => s.isVolunteer));
    // console.log('Sessions with volonteer factor', sessions.map(s => s.volunteerFactor).sort());
    // console.log('Sessions with link', sessions.filter(s => s.linkedWith));
    // console.log('Sessions with dependency', sessions.filter(s => s.dependsOn.length > 0));
    // console.log('Sessions with color', sessions.filter(s => s.colorDomain));
    // console.log('Trainer of sessions', sessions.filter(s => s.isTrainer));

  }
};

export default new Store();
