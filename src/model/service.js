import GenericElement from '../core/genericElement.js';
import store from './store.js';
import Moment from 'moment'

export default class SessionsService extends GenericElement {
  constructor() {
    super();

    this.store = store;

    this.subscribe({
      channel: 'days',
      topic: 'loadAll',
      callback: (data) => {
        this.loadDays();
      }
    });

    this.subscribe({
      channel: 'curriculums',
      topic: 'loadAll',
      callback: (data) => {
        // console.warn('loadCurriculum');
        this.loadCurriculums();
      }
    });

    this.subscribe({
      channel: 'sessions',
      topic: 'loadAll',
      callback: (data) => {
        // console.log('subscribe loadSessions');
        this.loadSessions();
      }
    });

    this.subscribe({
      channel: 'session',
      topic: 'register',
      callback: (data) => {
        this.register(data);
      }
    });

    this.subscribe({
      channel: 'session',
      topic: 'unregister',
      callback: (data) => {
        this.unregister(data);
      }
    });

    this.subscribe({
      channel: 'session',
      topic: 'loadOne',
      callback: (data) => {
        // console.log(this.getSelectedSession());
        this.selectSession(this.getSelectedSession());
      }
    });


    this.subscribe({
      channel: 'curriculums',
      topic: 'listAll.response',
      callback: (data) => {
        if (data.status === 200) {
          var curriculums = data.data;
          this.store.setCurriculums(curriculums);
          this.loadCurriculums();
        }
      }
    });

    this.subscribe({
      channel: 'auth',
      topic: 'login.response',
      callback: (data) => {
        // console.warn('auth',data);
        // this.store.ConnectedId=data.data.
        this.fetchUserData();
        this.fetchGlobalData();
      }
    });

    this.subscribe({
      channel: 'auth',
      topic: 'refresh.response',
      callback: (data) => {
        // console.warn('auth',data);
        // this.store.ConnectedId=data.data.
        this.fetchUserData();
        this.fetchGlobalData();
      }
    });

    this.subscribe({
      channel: 'auth',
      topic: 'loggedOut',
      callback: (data) => {
        this.store.cleanAll();
      }
    });

    this.subscribe({
      channel: 'users',
      topic: 'get',
      callback: () => {
        this.publish({
          channel: 'users',
          topic: 'changed',
          data: this.store.user
        });
      }
    })

    this.subscribe({
      channel: 'users',
      topic: 'get.connected',
      callback: () => {
        this.publish({
          channel: 'users',
          topic: 'changed.connected',
          data: this.store.connectedUser
        });
      }
    })

    this.subscribe({
      channel: 'sessions',
      topic: 'listAll.response',
      callback: (data) => {
        // console.warn('listAll.response');
        if (data.status === 200) {
          this.store.setSessions(data.data);
          // var sessions = data.data;
          // this.storeSessions(sessions);
          this.publishSessions();
        }
      }
    });

    this.subscribe({
      channel: 'sessions',
      topic: 'info.users.response',
      callback: (data) => {
        // console.warn('listAll.response');
        if (data.status === 200) {
          // this.store.setSessions(data.data);
          // var sessions = data.data;
          // this.storeSessions(sessions);
          let session = this.store.selectedSession;
          session.users = data.data;
          this.publish({
            channel: 'session',
            topic: 'loaded',
            data: session
          });
        }
      }
    });


    this.subscribe({
      channel: 'users',
      topic: 'update.response',
      callback: (data) => {
        //console.log('users', 'update.response', data);
        this.store.setUser(data.data);
        this.publish({
          channel: 'users',
          topic: 'loaded',
          data: data.data
        });
        this.publishSessions();
        //this.updatePresence(data.data.arrivalDate, data.data.leavingDate, data.data.ticketId);
      }
    });

    this.subscribe({
      channel: 'users',
      topic: 'info.response',
      callback: (data) => {
        console.log('info.response');
        if (data.status === 200) {
          // console.warn('info.response',data);
          // this.store.setRegistrations(data.data.sessions);
          this.store.setUser(data.data);
          this.publish({
            channel: 'users',
            topic: 'loaded',
            data: this.store.user
          });
          if (this.store.user.connectedSameSelected) {
            this.publish({
              channel: 'users',
              topic: 'connected.loaded',
              data: this.store.user
            });
          }
          this.publishSessions();
          // this.updatePresence(data.data.arrivalDate, data.data.leavingDate, data.data.ticketId);
        }
      }
    });

    this.subscribe({
      channel: 'menu',
      topic: 'loadAll',
      callback: () => {
        this.publishUserProgression();
      }
    });

    this.subscribe({
      channel: 'users',
      topic: 'register.response',
      callback: (data) => {
        if (data.status === 201) {
          // this.registred(data.id);
          // console.warn("registered");
          this.registredMulti(data.data.updatedSessions);
        } else if (data.status === 409) {
          this.publish({
            channel: 'session',
            topic: 'registration.failed',
            data: {
              id: data.id,
              reason: data.data.reason,
              affectedSessions: data.data.affectedSessions,
            }
          });
        } else {
          console.error(data);
          this.publish({
            channel: 'session',
            topic: 'registration.failed',
            data: data
          });
        }
      }
    });

    this.subscribe({
      channel: 'users',
      topic: 'unregister.response',
      callback: (data) => {
        if (data.status === 200) {
          // this.unregistred(data.id);
          this.unregistredMulti(data.data.updatedSessions);
        } else if (data.status === 409) {
          this.publish({
            channel: 'session',
            topic: 'registration.failed',
            data: {
              id: data.id,
              reason: data.data.reason,
              affectedSessions: data.data.affectedSessions,
            }
          });
        } else {
          console.error(data);
          this.publish({
            channel: 'session',
            topic: 'registration.failed',
            data: data
          });
        }
      }
    });

    this.subscribe({
      channel: 'agenda',
      topic: 'loadAll',
      callback: () => {
        this.publishAgenda();
      }
    });

    this.subscribe({
      channel: 'users',
      topic: 'select.request',
      callback: (data) => {
        console.log('select.request');
        this.store.setUserWithoutImpact(data);
        this.store.cleanSession();
        this.fetchUserData(true);
      }
    })
  }

  publishSessions() {
    if (this.store.user != undefined && this.store.sessions != undefined) {
      this.loadDays();
      this.loadSessions();
      this.publishUserProgression();
      this.publishAgenda();
      this.loadSelectedSession();
    }
  }

  fetchUserData(force = false) {
    // console.log(this.store.isEmpty());

    if (this.store.isEmpty() || force) {
      // console.log('fetchUserData');
      this.publish({
        channel: 'sessions',
        topic: 'listAll.request'
      });
      this.publish({
        channel: 'users',
        topic: 'info.request',
        data: {}
      });
    }
  }

  fetchGlobalData() {
    if (this.store.curriculums == undefined) {
      this.publish({
        channel: 'curriculums',
        topic: 'listAll.request'
      });
    } else {
      this.loadCurriculums();
    }
  }





  publishUserProgression() {
    this.publish({
      channel: 'volunteerPercent',
      topic: 'updated',
      data: this.store.getVolunteerPercent()
    });
    this.publish({
      channel: 'averageActivity',
      topic: 'updated',
      data: this.store.getAverageActivity()
    });
  }

  loadDays() {
    // console.log('publish loadDays');
    this.publish({
      channel: 'daysWithInfo',
      topic: 'loaded',
      data: {
        days: this.store.presenceDays.map(d => {
          return this.getDayWithInfo(d);
        }),
        baseUrl: '#/x-register?',
        selectedDayIndex: this.getSelectedDayIndex(),
      }
    });
  }

  loadSessions() {
    if (this.store.sessions != undefined && this.store.user != undefined) {
      // console.log('start loadSessions');
      let selectedDayIndex = this.getSelectedDayIndex();
      let selectedDaySessions = this.store.getSessionsByDayIndex(selectedDayIndex);
      // console.warn("loadSessions",selectedDaySessions);
      this.publish({
        channel: 'sessions',
        topic: 'loaded',
        data: selectedDaySessions
      });
    }
  }
  selectSession(keySlot) {
    this.store.selectSession(keySlot);
    if (this.store.sessions != undefined) {
      this.loadSelectedSession();
    }
  }

  loadSelectedSession() {
    this.store.applySelectedKeySlot();
    this.publish({
      channel: 'sessions',
      topic: 'info.users.request',
      data: {
        sessionKey: this.store.selectedKeySlot
      }
    });
  }

  loadCurriculums() {
    this.publish({
      channel: 'curriculums',
      topic: 'loaded',
      data: this.store.curriculums
    });
  }

  getSelectedSession() {
    let regex = /\#\/x-session-detail\?.*session=(.+)\/?/ig;
    let regExec = regex.exec(document.location.hash);
    if (regExec) {
      let session = regExec[1];
      return session;
    }
    return null;
  }

  getSelectedDayIndex() {
    let regex = /\#\/x-register\?.*day=(\d+)\/?/ig;
    let regExec = regex.exec(document.location.hash);
    if (regExec) {
      let selected = parseInt(regExec[1]);
      if (Number.isInteger(selected)) {
        return selected;
      }
    }
    return null;
  }

  getDayWithInfo(day) {
    let specialBuild = false;
    // console.log('getSelectedDayWithInfo',day,this.store.startDayVolunteer,this.store.endDayVolunteer);
    if (day.isSame(this.store.startDayVolunteer, 'day') || day.isSame(this.store.endDayVolunteer, 'day')) {
      specialBuild = true;
    }
    return {
      specialBuild: specialBuild,
      day: day
    }
  }

  getSelectedDayWithInfo() {
    console.log('PUTTAIN');
    if (this.getSelectedDayIndex() != undefined && this.store.presenceDays.length > 0) {
      let index = this.getSelectedDayIndex();
      let day = this.store.presenceDays[index - 1];
      return this.getDayWithInfo(day);

    } else {
      return undefined
    }
  }

  register(sessionKey) {
    this.publish({
      channel: 'users',
      topic: 'register.request',
      data: {
        sessionKey: sessionKey
      }
    });
  }

  registred(sessionKey) {
    var updatedSessions = this.store.sessionRegistred(sessionKey);
    this.publishSessionsChange(updatedSessions);
  }

  registredMulti(sessionKeys) {
    sessionKeys.forEach(sessionKey => {
      this.registred(sessionKey);
    })
  }

  unregister(sessionKey) {
    var errorMessage = this.store.isUnregistrationForbidden(sessionKey);
    if (errorMessage) {
      this.publish({
        channel: 'session',
        topic: 'registration.failed',
        data: {
          key: sessionKey,
          message: errorMessage
        }
      });
    } else {
      this.publish({
        channel: 'users',
        topic: 'unregister.request',
        data: {
          sessionKey: sessionKey
        }
      });
    }
  }

  unregistredMulti(sessionKeys) {
    sessionKeys.forEach(sessionKey => {
      this.unregistred(sessionKey);
    })
  }

  unregistred(sessionKey) {
    var updatedSessions = this.store.sessionUnregistred(sessionKey);
    this.publishSessionsChange(updatedSessions);
  }

  publishSessionsChange(sessions) {
    sessions.forEach((session) => {
      this.publish({
        channel: 'session',
        topic: 'changed',
        data: session
      });
    });
    this.publishUserProgression();
    this.publishAgenda();
  }

  publishAgenda() {
    this.publish({
      channel: 'agenda',
      topic: 'updated',
      data: this.store.registeredSessions
    });
  }
}
window.customElements.define('x-service-sessions', SessionsService);
