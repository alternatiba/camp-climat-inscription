import GenericElement from '../core/genericElement.js';
import store from './store.js';
import * as request from "request";


const urlEnv=process.env.CAMP_CLIMAT_API_URL
const url = urlEnv==undefined?"https://camp-climat-api.herokuapp.com/v1.0":urlEnv;
const rooms_url = "https://grappe.io/data/api/5d30d48afccfdc00a0e41207-rooms"
console.log(url);

function isValidJSON(str) {
  try {
      JSON.parse(str);
  } catch (e) {
      return false;
  }
  return true;
}

export default class ApiService extends GenericElement {

  constructor() {
    super();
    this.store=store;

    this.jwt_token = localStorage.getItem('jwt_token');

    this.subscribe({
      channel: 'auth',
      topic: 'login.request',
      callback: (data) => {
        var options = {
          method: 'POST',
          url: `${url}/auth/password`,
          json: true,
          body: {
            email: data.email,
            password: data.password,
            cookie: data.cookie,
          }
        }
        return this.basicRequest(options, 'auth', 'login', (status, body) => {
          // if (status == 200) {
          //   this.jwt_token = body.jwt_token;
          //   localStorage.setItem('jwt_token', this.jwt_token);
          // }
        });
      }
    });

    this.subscribe({
      channel: 'auth',
      topic: 'login.response',
      callback: (data) => {
        console.warn('login.response',data);
        if(data.status==200){
          this.jwt_token = data.data.jwt_token;
          localStorage.setItem('jwt_token', this.jwt_token);
        }else if(data.status==401){
          localStorage.removeItem('jwt_token');
          // No logout because login page have tohold
          // this.publish({channel: "auth", topic: "loggedOut"});
        }
      }
    });


    this.subscribe({
      channel: 'auth',
      topic: 'refresh.request',
      callback: (data) => {

        var options = {
          method: 'POST',
          url: `${url}/auth/refresh`
        }
        return this.loggedInRequest(options, 'auth', 'refresh', (status, body) => {

        });
      }
    });

    this.subscribe({
      channel: 'auth',
      topic: 'refresh.response',
      callback: (data) => {
        console.warn('refresh.response',data);
        if(data.status==200){
          this.jwt_token = data.data.jwt_token;
          localStorage.setItem('jwt_token', this.jwt_token);
        }else if(data.status==401){
          localStorage.removeItem('jwt_token');
          this.publish({channel: "auth", topic: "loggedOut"});
        }

      }
    });

    this.subscribe({
      channel: 'auth',
      topic: 'logout',
      callback: (data) => {
          this.jwt_token = null;
          localStorage.removeItem('jwt_token');
          this.publish({channel: "auth", topic: "loggedOut"});
      }
    });


    this.subscribe({
      channel: 'auth',
      topic: 'forgot.request',
      callback: (data) => {
        var options = {
          method: 'POST',
          url: `${url}/auth/password/forgot`,
          json: true,
          body: {email: data.email},
        }
        return this.basicRequest(options, 'auth', 'forgot');
      }
    });

    this.subscribe({
      channel: 'auth',
      topic: 'reset.request',
      callback: (data) => {
        var options = {
          method: 'POST',
          url: `${url}/auth/password/reset`,
          json: true,
          body: {
            password: data.password,
            token: data.token,
          },
        }
        return this.basicRequest(options, 'auth', 'reset');
      }
    });

    this.subscribe({
      channel: 'helloasso',
      topic: 'info.request',
      callback: (data) => {
        var options = {
          method: 'GET',
          url: `${url}/helloasso/info/${data.helloassoId}`,
          json: true,
        }
        return this.basicRequest(options, 'helloasso', 'info');
      }
    });

    this.subscribe({
      channel: 'users',
      topic: 'signup.request',
      callback: (data) => {
        var options = {
          method: 'POST',
          url: `${url}/users`,
          json: true,
          body: {
            ticketId: data.helloassoId,
            email: data.email,
            password: data.password,
          }
        }
        return this.basicRequest(options, 'users', 'signup');
      }
    });

    this.subscribe({
      channel: 'users',
      topic: 'info.request',
      callback: (data) => {
        console.warn('info.request',data.userId,store.user);
        let userId=this.getUserIdUrl(data.userId);
        // if(data.userId){
        //   userId=data.userId;
        // }else if(store.user!=undefined) {
        //   userId=store.user.id;
        // }else{
        //   userId='me';
        // }

        var options = {
          method: 'GET',
          url: `${url}/users/${userId}`
        }
        // this.loggedInRequest(options, 'users', 'info');
        this.loggedInRequest(options, 'users', 'info', (status, body) => {
          if (status == 200) {
            if (userId == 'me') {
              // console.warn(id, typeof id);
              store.connectedUserId=body.id.toString();
              // userService.setUserMeId(body.id);
              // userService.setAdmin(body.isAdmin);
            }
            // userService.updateUser(userId, body.email);
            // this.publish({
            //   channel: 'user.service',
            //   topic: 'updated',
            // })
          }
        });
      }
    });

    this.subscribe({
      channel: 'users',
      topic: 'search.request',
      callback: (data) => {
        var query = data.query || '';
        var options = {
          method: 'GET',
          url: `${url}/users/search?q=${query}`
        }
        return this.loggedInRequest(options, 'users', 'search');
      }
    });


    this.subscribe({
      channel: 'users',
      topic: 'update.request',
      callback: (data) => {
        // var userId = data.userId || userService.getUserId() || 'me';
        let userId=this.getUserIdUrl(data.userId);
        var options = {
          method: 'PATCH',
          url: `${url}/users/${userId}`,
          json: true,
          body: {
            password: data.password, // if set all others are ignored
            firstname: data.firstname,
            lastname : data.lastname,
            locale: data.locale,
            arrivalDate: data.arrivalDate.toJSON(),
            leavingDate: data.leavingDate.toJSON(),
            comment: data.comment,
            tel: data.phoneNumber,
            origin: data.origin,
            wantPrint: data.wantPrint,
            curriculums:data.curriculums,
            paid:data.paid,
          }
        }
        return this.loggedInRequest(options, 'users', 'update');
      }
    });

    this.subscribe({
      channel: 'users',
      topic: 'register.request',
      callback: (data) => {
        // var userId = data.userId || userService.getUserId() || 'me';
        let userId=this.getUserIdUrl(data.userId);
        var options = {
          method: 'POST',
          url: `${url}/users/${userId}/register`,
          json: true,
          body: {
            session: data.sessionKey,
            //isTrainer: data.isTrainer || false
          }
        }
        return this.loggedInRequest(options, 'users', 'register', null, data.sessionKey);
      }
    });

    this.subscribe({
      channel: 'users',
      topic: 'unregister.request',
      callback: (data) => {
        // var userId = data.userId || userService.getUserId() || 'me';
        let userId=this.getUserIdUrl(data.userId);
        var options = {
          method: 'POST',
          url: `${url}/users/${userId}/unregister`,
          json: true,
          body: {
            session: data.sessionKey,
            //isTrainer: data.isTrainer || false
          }
        }
        return this.loggedInRequest(options, 'users', 'unregister', null, data.sessionKey);
      }
    });

    this.subscribe({
      channel: 'pdf',
      topic: 'volunteer.request',
      callback: (data) => {
        var options = {
          method: 'GET',
          encoding: null,
          url: `${url}/pdf/volunteer/${data.date}`
        };
        return this.loggedInRequest(options, 'pdf', 'volunteer');
      }
    });

    this.subscribe({
      channel: 'pdf',
      topic: 'rooms.request',
      callback: (data) => {
        var options = {
          method: 'GET',
          encoding: null,
          url: `${url}/pdf/rooms`
        };
        return this.loggedInRequest(options, 'pdf', 'rooms');
      }
    });

    this.subscribe({
      channel: 'pdf',
      topic: 'planning.request',
      callback: (data) => {
        let userId = this.getUserIdUrl(data.userId);
        var options = {
          method: 'GET',
          encoding: null,
          url: `${url}/pdf/planning/${userId}`
        };
        return this.loggedInRequest(options, 'pdf', 'planning');
      }
    });

    this.subscribe({
      channel: 'pdf',
      topic: 'referents.request',
      callback: (data) => {
        var options = {
          method: 'GET',
          encoding: null,
          url: `${url}/pdf/referents`
        };
        return this.loggedInRequest(options, 'pdf', 'referents');
      }
    });

    this.subscribe({
      channel: 'pdf',
      topic: 'session.request',
      callback: (data) => {
        var options = {
          method: 'GET',
          encoding: null,
          url: `${url}/pdf/session/${data.keySession}`
        };
        return this.loggedInRequest(options, 'pdf', 'session');
      }
    });

    this.subscribe({
      channel: 'sessions',
      topic: 'listAll.request',
      callback: (data) => {
        console.warn('listAll.request');
        var options = {
          method: 'GET',
          url: `${url}/sessions`
        }
        return this.loggedInRequest(options, 'sessions', 'listAll');
      }
    });

    this.subscribe({
      channel: 'sessions',
      topic: 'info.users.request',
      callback: (data) => {
        var options = {
          method: 'GET',
          url: `${url}/sessions/${data.sessionKey}/users`
        }
        return this.loggedInRequest(options, 'sessions', 'info.users');
      }
    });

    this.subscribe({
      channel: 'curriculums',
      topic: 'listAll.request',
      callback: (data) => {
        var options = {
          method: 'GET',
          url: `${url}/curriculums`
        }
        return this.loggedInRequest(options, 'curriculums', 'listAll');
      }
    });

    this.subscribe({
      channel: 'rooms',
      topic: 'loadAll',
      callback: (data) => {
        var options = {
          method: 'GET',
          url: `${rooms_url}`
        }
        return this.basicRequest(options, 'rooms', 'loadAll');
      }
    });
  }

  basicRequest(req_options, channel, topic, callback) {
    request(req_options, (err,res,body) => {
      if (err) {
        return this.publish({
          channel: channel,
          topic: `${topic}.response`,
          data: {
            status: 500,
            error: err
          }
        })
      }
      if (body) {
        if (isValidJSON(body)) {
          body = JSON.parse(body)
        }
        if (callback) {
          callback(res.statusCode, body);
        }
        return this.publish({
          channel: channel,
          topic: `${topic}.response`,
          data: {
            status: res.statusCode,
            data: body
          }
        })
      }
      return this.publish({
        channel: channel,
        topic: `${topic}.response`,
        data: {
          status: res.statusCode,
        }
      })
    })
  }

  loggedInRequest(req_options, channel, topic, callback, id) {
    if (!this.jwt_token) {
      return this.publish({
        channel: channel,
        topic: `${topic}.response`,
        data: {
          status: 401,
        }
      })
    }
    if (!req_options.headers) {
      req_options.headers = {}
    }
    req_options.headers = Object.assign(req_options.headers, {
      'Authorization': `JWT ${this.jwt_token}`,
    })
    request(req_options, (err,res,body) => {
      if (err) {
        return this.publish({
          channel: channel,
          topic: `${topic}.response`,
          data: {
            status: 500,
            error: err,
            id: id,
          }
        })
      }

      if (callback) {
        if (isValidJSON(body)) {
          body = JSON.parse(body)
        }
        callback(res.statusCode, body);
      }
      if (body) {
        if (isValidJSON(body)) {
          body = JSON.parse(body)
        }
        this.publish({
          channel: channel,
          topic: `${topic}.response`,
          data: {
            status: res.statusCode,
            data: body,
            id: id,
          }
        })
      }else{
        this.publish({
          channel: channel,
          topic: `${topic}.response`,
          data: {
            status: res.statusCode,
            id: id,
          }
        })
      }

    })
  }

  getUserIdUrl(userIdClaim){
    let userId;
    if(userIdClaim!=undefined){
      userId=userIdClaim;
    }else if(store.user!=undefined) {
      userId=store.user.id;
    }else{
      userId='me';
    }
    return userId;
  }

  download(data, filename, type) {
    var blob = new Blob([data], {type: type});
    saveAs(blob, filename);
  }
}
window.customElements.define('x-service-api', ApiService);
