import postal from 'postal';
import shared from '../style/shared.css';
import containers from '../style/containers.css';
export default class GenericElement extends HTMLElement {
  constructor(view, cssArray) {
    super();
    this.subscriptions = [];
    this.propagatedStyle = [];
    this.genericElementChildren = [];

    if (view != undefined) {
      this.attachShadow({
        mode: 'open'
      });
      this.shadowRoot.innerHTML = view;
      if (cssArray == undefined) {
        cssArray = [shared, containers];
      }
      // console.log('cssArray',cssArray.length);
      cssArray.forEach(css => {
        let injectedStyle = document.createElement('style');
        injectedStyle.appendChild(document.createTextNode(css.toString()));
        this.shadowRoot.appendChild(injectedStyle)
      })

    }

    // new MutationObserver((mutationsList) => {
    //   mutationsList.forEach(mutation => {
    //     mutation.addedNodes.forEach(addedNode => {
    //       if (addedNode.tagName == "STYLE" && !addedNode.classList.contains('notToObserve')) {
    //         console.log('MutationObserver');
    //         // console.log('MutationObserver',this,addedNode.classList);
    //         let injectedStyle = document.createElement('style');
    //         injectedStyle.appendChild(document.createTextNode(addedNode.innerText));
    //         // let clone = injectedStyle.cloneNode(true);
    //         // injectedStyle.classList.add('notToObserve');
    //         this.appendPropagatedStyle(injectedStyle);
    //       }
    //       addedNode.remove();
    //     })
    //   })
    // }).observe(this, {
    //   attributes: false,
    //   childList: true,
    //   subtree: false
    // });

  }

  addGenericElementChildren(child) {
    // this.genericElementChildren.push(child);
    // this.propagatedStyle.forEach(injectedStyleSource=>{
    //   child.appendPropagatedStyle(injectedStyleSource);
    // })
  }

  appendPropagatedStyle(injectedStyleSource) {
    // let clone = injectedStyleSource.cloneNode(true);
    // clone.classList.add('notToObserve');
    // this.propagatedStyle.push(clone);
    // this.shadowRoot.appendChild(clone);

  }

  spreadPropagatedStyle(injectedStyle) {
    // this.genericElementChildren.forEach(child=>{
    //   child.spreadPropagatedStyle(injectedStyle);
    // })
  }



  connectedCallback() {
    // this.host=this.getRootNode().host;
    // if (this.getRootNode().host instanceof GenericElement){
    //   this.host.addGenericElementChildren(this);
    // }else{
    // }
  }

  disconnectedCallback() {
    this.subscriptions.forEach(s => {
      s.unsubscribe();
    })
  }

  attributeChangedCallback(attrName, oldVal, newVal) {}

  subscribe(options) {
    this.subscriptions.push(postal.subscribe(options))
  }

  publish(options) {
    postal.publish(options);
  }

}
